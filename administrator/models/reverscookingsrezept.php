<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Thm_reverscookings model.
 */
class Thm_reverscookingsModelreverscookingsrezept extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 */
	protected $text_prefix = 'COM_THM_REVERSCOOKINGS';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	
	 */
	public function getTable($type = 'Reverscookingsrezept', $prefix = 'Thm_reverscookingsTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_thm_reverscookings.reverscookingsrezept', 'reverscookingsrezept', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.

	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_thm_reverscookings.edit.reverscookingsrezept.data', array());

		if (empty($data)) {
			$data = $this->getItem();
            
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {

			//Do any procesing on fields here if needed

		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 */
	protected function prepareTable(&$table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__thm_reverscookings_rezept');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}
	public static function getIngredient($itemid){
		$result = '<div id ="content-ing" ><table ><tr><th>Name</th><th>quantity</th><th>unit</th><th>Option</th></tr>';
		$db = JFactory::getDBO();
		$query = 'SELECT rezeptingredient.id AS id, ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, rezeptingredient.quantity AS ingquantity
		FROM  #__thm_reverscookings_ingredients_rezept AS rezeptingredient LEFT JOIN  #__thm_reverscookings_rezept AS rezepts
		ON rezeptingredient.rezeptid = rezepts.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON rezeptingredient.ingid = ingredients.id WHERE rezepts.id = '.$itemid;
		$db->setQuery($query);
		$ingredients = $db->loadObjectList();
		if(!empty($ingredients)){
			foreach ($ingredients as $ing){
				$del= 'delete'.$ing->id;
				$result .='<tr><td>'.$ing->ingname.'</td><td><input type="text" id="quantity"  name= ingredient['.$ing->id.']'.' value='.$ing->ingquantity.' /></td>
				<td>'.$ing->ingunit.'</td><td><button  id='.$del.'  value='.$itemid.' onclick="deleteing('.$ing->id.')">delete</button></td></tr>';
			}
		}
		$result .='</table></div>';
	
		$result .= '<div id = "fields-container"></div>';
	
		$result.='<li><label>Ingredient Einfuegen</label><td>
		<input type="text" onkeyup="autocom()"  value="name " id="ingname" maxlength="255"></td>
		<td><input type="text" value="quantity" id="ingquantity" maxlength="15"></td>
		<td><input type="text" value="unit" id="ingunit" maxlength="15"></td>
		<td><button type="button" id="einfuegen" value="einfuegen" onClick="runButton()">Einfuegen</button></td></li>';
		$result.='<div id="ing-container" style="display:block align:center"></div></td>';
	
	
		return $result;
	}
	public static function getComments($id,$createdby){
		$akuser = JFactory::getUser();
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->select('a.*')->from('#__thm_reverscookings_comment_rezept as a');
		$query->select('u.name AS autor');
		$query->join('LEFT', '#__users AS u ON u.id = a.userid');
		$query->select('r.namerezept AS rezept_name');
		$query->join('LEFT', '#__thm_reverscookings_rezept AS r ON a.rezeptid = r.id');
		$query->where('a.rezeptid = '.(int) $id);
		$db->setQuery($query);
		$comments = $db->loadObjectList();
		if(!empty($comments)){
			foreach ($comments as $comment){
				if(($db->escape($comment->comment, true)) == NULL)
					continue;
				$result .= '</br><div><p>'. $comment->comment.'</p></br>'.JText::_('COM_CODE102'). $comment->autor. '</br>'.JText::_('COM_CODE103').': '.$comment->date_create;
			
				if(isset($akuser->groups[8]) || isset($akuser->groups[7])){
					$result .='<form name= "deletecomment" method="post" action= "'. JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.deletecomment').'" >';
					$result .= '<input type="hidden" name="user_id" value="'.$akuser.'" >';
					$result .= '<input type="hidden" name="commentid" value="'.$comment->id.'" >';
					$result .= '<input type="hidden" name="rezeptid" value="'.$id.'" >';
					$result.='<input id= "rating-post" type= "submit" value= "'. JText::_('COM_CODE121').'"></form>';
				}
	
				$result.='</br>';
			}
			$result .= '</div>';
		}
		return $result;
	}

}