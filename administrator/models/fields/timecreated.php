<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldTimecreated extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 */
	protected $type = 'timecreated';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = array();
        
		$time_created = $this->value;
		if (!$time_created || strcmp( $time_created, "0000-00-00 00:00:00")==0) {
			$time_created = date("Y-m-d H:i:s");
		}
		$html[] = '<input type="hidden" name="'.$this->name.'" value="'.$time_created.'" />';
		$jdate = new JDate($time_created);
		$pretty_date = $jdate->format(JText::_('DATE_FORMAT_LC2'));
		$html[] = "<div>".$pretty_date."</div>";
        
		return implode($html);
	}
}