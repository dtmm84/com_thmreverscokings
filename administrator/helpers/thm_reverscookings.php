<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
/**
 * Thm_reverscookings helper.
 */
class Thm_reverscookingsHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JSubMenuHelper::addEntry(
			JText::_('REVERSCOOKINGSINGREDIENTS'),
			'index.php?option=com_thm_reverscookings&view=reverscookingsingredients',
			$vName == 'reverscookingsingredients'
		);
		JSubMenuHelper::addEntry(
			JText::_('COM_KATEGORIE_ZUTATEN'),
			'index.php?option=com_categories&extension=com_thm_reverscookings.ingkategory',
			$vName == 'categories.ingkategory'
		);
		
		JSubMenuHelper::addEntry(
				JText::_('REVERSCOOKINGSPROFILS'),
				'index.php?option=com_thm_reverscookings&view=reverscookingsprofils',
				$vName == 'reverscookingsprofils'
		);
		JSubMenuHelper::addEntry(
				JText::_('REVERSCOOKINGSREZEPTS'),
				'index.php?option=com_thm_reverscookings&view=reverscookingsrezepts',
				$vName == 'reverscookingsrezepts'
		);
		JSubMenuHelper::addEntry(
				JText::_('COM_KATEGORIE_REZEPT'),
				'index.php?option=com_categories&extension=com_thm_reverscookings.rezeptkategory',
				$vName == 'categories.rezeptkategory'
		);
		
		
		if ($vName=='categories.ingkategory') {
			JToolBarHelper::title('Thm Reverscookings: Categories (Ingkategory)');
		}
		
		if ($vName=='categories.ingkategory') {			
		JToolBarHelper::title('Thm Reverscookings: Categories (Ingkategory)');		
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_thm_reverscookings';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	
	//give the max id of recpet Table
	public static function maxid($datenbanktabel){
		$db = JFactory::getDBO();
		$ingredientquery='SELECT MAX(a.id) AS id FROM '.$datenbanktabel.' AS a' ;
		$db->setQuery($ingredientquery);
		$db->query();
		return  $db->loadObjectList();
	}
	public static function  inglikename($teilname){
		$temp = '%'.$teilname.'%';
		$db = JFactory::getDBO();
		$ingredientquery="SELECT * FROM `#__thm_reverscookings_ingredients` WHERE ingname LIKE '$temp'";
		$db->setQuery($ingredientquery);
		$db->query();
		return  $db->loadObjectList();
	}
	public static function  deleteingrezept($id){
		$db = JFactory::getDBO();
		$deletingrezept = 'DELETE FROM #__thm_reverscookings_ingredients_rezept  WHERE id='.$id;
		$db->setQuery($deletingrezept);
		$db->query();
		return true;
	}
	public static function newinglist($rezeptid){
		$db = JFactory::getDBO();
		$result='<table ><tr><th>Name</th><th>quantity</th><th>unit</th><th>Option</th></tr>';
		
		$query = 'SELECT rezeptingredient.id AS id, ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, rezeptingredient.quantity AS ingquantity
		FROM  #__thm_reverscookings_ingredients_rezept AS rezeptingredient LEFT JOIN  #__thm_reverscookings_rezept AS rezepts
		ON rezeptingredient.rezeptid = rezepts.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON rezeptingredient.ingid = ingredients.id WHERE rezepts.id ='.$rezeptid;
		$db->setQuery($query);
		$db->query();
		$ingredients = $db->loadObjectList();
		if(!empty($ingredients)){
			foreach ($ingredients as $ing){
				$del= 'delete'.$ing->id;
				$result .='<tr><td>'.$ing->ingname.'</td><td><input type="text" id="quantity"  name= ingredient['.$ing->id.']'.' value='.$ing->ingquantity.' /></td>
				<td>'.$ing->ingunit.'</td><td><button  id='.$del.'  value='.$rezeptid.' onclick="deleteing('.$ing->id.')">delete</button></td></tr>';
			}
		}
		$result .='</table>';
		return $result;
	}
	//Update the time for a Rezept in Frontend
	public static function updatetime($id, $datetime,$tabel){
		$db =& JFactory::getDbo();
		$update="UPDATE  ".$tabel."  SET checked_out_time ='$datetime'  WHERE id=".$id;
		$db->setQuery($update);
		$db->query();
	
	}
	public static function hasbild($datenbanktabel,$element, $element_value){
		$db = JFactory::getDbo();
		$query = "SELECT * FROM ".$datenbanktabel." WHERE ".$element." = '$element_value'";
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if(empty($result))
			return false;
		return $result;
	
	}
	public static function savebild($user_id, $element_value, $isrezept){
		$file = JRequest::getVar('image_upload', null, 'files', 'array');
		$db = JFactory::getDbo();
		var_dump($file['name']);
		$filename = JFile::makeSafe($file['name']);
		$filesize = $file['size'] /1024;
		$src = $file['tmp_name'];
		$format = JFile::getExt($filename);
		$newfilename = $element_value.'_'.$user_id.'.'.$format;
		$dest = JPATH_SITE . DS . 'components' . DS . 'com_thm_reverscookings' . DS . 'img' . DS . 'profil_bild'.DS.$newfilename;
		$datenbanktabel = '#__thm_reverscookingsprofils_bild';
		$element = 'profilid';
		if($isrezept == true){
			$dest = JPATH_SITE . DS . 'components' . DS . 'com_thm_reverscookings' . DS . 'img' . DS . 'rezept_bild'.DS.$newfilename;
			$datenbanktabel = '#__thm_reverscookings_rezept_bilder';
			$element = 'rezept_id';
		}
		unlink($dest);
		if ( $format== 'jpg' || $format == 'gif' ||$format =='png' || $format =='jpeg' || $format =='swf') {
			if ( JFile::upload($src, $dest) ) {
				$query ='';
				if(	self::hasbild($datenbanktabel, $element, $element_value) == false){
					$query="INSERT ".$datenbanktabel." (".$element.",user_id , size , format , filename)
					VALUES ('$element_value' , '$user_id' , '$filesize',' $format','$newfilename')";
				}
				else{
					$query = "UPDATE ".$datenbanktabel."
					SET size  ='$filesize' , format ='$format' , filename = '$newfilename' WHERE ".$element." =".$element_value;
				}
				$db->setQuery($query);
				$db->query();
				return true;
			}
		} else {
			return "NF";
		}
	}
	public static function savevideo($user_id, $element_value){
		$file = JRequest::getVar('video_upload', null, 'files', 'array');
		$db = JFactory::getDbo();
		$filename = JFile::makeSafe($file['name']);
		$filesize = $file['size'] /(1024*2);
		$src = $file['tmp_name'];
		var_dump($file);
		$format = JFile::getExt($filename);
		$newfilename = $element_value.'_'.$user_id.'.'.$format;
		$dest = JPATH_SITE . DS . 'components' . DS . 'com_thm_reverscookings' . DS . 'img' . DS . 'rezept_video'.DS.$newfilename;
		$datenbanktabel = '#__thm_reverscookings_rezept_video';
		$element = 'rezept_id';
	
		unlink($dest);
		if ( $format== 'flv' || $format == 'mp4'||$format == 'wmv'  ) {
			if ( JFile::upload($src, $dest) ) {
				$query ='';
				if(	self::hasbild($datenbanktabel, $element, $element_value) == false){
					$query="INSERT ".$datenbanktabel." (".$element.",user_id , size , format , filename)
					VALUES ('$element_value' , '$user_id' , '$filesize',' $format','$newfilename')";
				}
				else{
					$query = "UPDATE ".$datenbanktabel."
					SET size  ='$filesize' , format ='$format' , filename = '$newfilename' WHERE ".$element." =".$element_value;
				}
				$db->setQuery($query);
				$db->query();
				return true;
			}
		} else {
			return "NF";
		}
	}
	// delete Image oder Video
	public static function deletedata($elementId,$attribut, $basetable){
		$db = JFactory::getDbo();
		$query = "SELECT filename FROM ".$basetable." WHERE ".$atribut." =".$elementId;
		$db->setQuery($query);
		$data = $db->loadObjectList();
		$filename = $data[0]->filename;
		unlink($filename);
		$query = "DELETE FROM ".$basetable." WHERE ".$attribut." = ".$elementId;
		$db->setQuery($query);
		$db->query();
	}
	//Calculate the view for a Rezept
	public static function viewrezept($rezeptid){
		if($rezeptid == 0)
			return;
		$db =& JFactory::getDbo();
		$query='SELECT views as value FROM #__thm_reverscookings_rezept WHERE id='.$rezeptid;
		$db->setQuery($query);
		$db->query();
		$view = $db->loadObjectList();
		$intview= intval($view[0]->value);
		$result = $intview + 1;
		$update='UPDATE #__thm_reverscookings_rezept  SET views ='.$result.' WHERE id='.$rezeptid;
		$db->setQuery($update);
		$db->query();
		return $result;
	}
	public static function getRating($id){
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = 'SELECT AVG(bewertung) AS bewertung
		FROM  #__thm_reverscookings_bewertung_rezept
		WHERE rezeptid = '.$id;
		$db->setQuery($query);
		$ratings = $db->loadObjectList();
		if(!empty($ratings)){
			foreach ($ratings as $rating){
				$result = $rating->bewertung;
			}
		}
	
		$result .= '</div>';
		return round($result,2);
	}
}
