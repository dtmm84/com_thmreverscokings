<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.view');

class Thm_reverscookingsViewthm_reverscookings extends JView{

	protected  $items;

	public function display($tpl = null){

		$this->addToolbar();
		$this->items = $this->get('Items');
		parent::display();

	}

	protected function  addToolbar(){

		JToolBarHelper::title(JText::_('REVERSECOOKINGS_HOME'));
	


	}
}