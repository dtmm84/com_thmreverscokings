<?php
/**
 * @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_thm_reverscookings/assets/css/thm_reverscookings.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'reverscookingsprofil.cancel' || document.formvalidator.isValid(document.id('reverscookingsprofil-form'))) {
			Joomla.submitform(task, document.getElementById('reverscookingsprofil-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="reverscookingsprofil-form" class="form-validate"   enctype="multipart/form-data" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_THM_REVERSCOOKINGS_LEGEND_REVERSCOOKINGSPROFIL'); ?></legend>
			<ul class="adminformlist">
                
				<li><?php echo $this->form->getLabel('uid'); ?>
				<?php echo $this->form->getInput('uid'); ?></li>
				<li><?php echo $this->form->getLabel('checked_out_time'); ?>
				<?php echo $this->form->getInput('checked_out_time'); ?></li>
				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>
				<li><?php echo $this->form->getLabel('created_by'); ?>
				<?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('uname'); ?>
				<?php echo $this->form->getInput('uname'); ?></li>
				<li><?php echo $this->form->getLabel('address'); ?>
				<?php echo $this->form->getInput('address'); ?></li>
				<li><?php echo $this->form->getLabel('plz'); ?>
				<?php echo $this->form->getInput('plz'); ?></li>
				<li><?php echo $this->form->getLabel('ort'); ?>
				<?php echo $this->form->getInput('ort'); ?></li>
				<li><?php echo $this->form->getLabel('email'); ?>
				<?php echo $this->form->getInput('email'); ?></li>
				<li><?php echo $this->form->getLabel('bild'); ?>
				<input type="file" name = "image_upload" /></li>
				<li><?php echo $this->form->getLabel('virtualfridge'); ?>
				<?php echo $this->form->getInput('virtualfridge'); ?></li>
				<li><?php echo $this->form->getLabel('description'); ?>
				<?php echo $this->form->getInput('description'); ?></li>


            </ul>
		</fieldset>
	</div>


	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>