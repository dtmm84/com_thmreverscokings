<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_thm_reverscookings/assets/css/thm_reverscookings.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'reverscookingsingredient.cancel' || document.formvalidator.isValid(document.id('reverscookingsingredient-form'))) {
			Joomla.submitform(task, document.getElementById('reverscookingsingredient-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsingredient&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="reverscookingsingredient-form" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_THM_REVERSCOOKINGS_LEGEND_REVERSCOOKINGSINGREDIENT'); ?></legend>
			<ul class="adminformlist">
                
				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>
				<li><?php echo $this->form->getLabel('created_by'); ?>
				<?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('ingname'); ?>
				<?php echo $this->form->getInput('ingname'); ?></li>
				<li><?php echo $this->form->getLabel('ingunit'); ?>
				<?php echo $this->form->getInput('ingunit'); ?></li>
				<li><?php echo $this->form->getLabel('ingkategory'); ?>
				<?php echo $this->form->getInput('ingkategory'); ?></li>
				<li><?php echo $this->form->getLabel('checked_out_time'); ?>
				<?php echo $this->form->getInput('checked_out_time'); ?></li>


            </ul>
		</fieldset>
	</div>


	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>