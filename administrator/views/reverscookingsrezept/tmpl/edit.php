<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>  
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_thm_reverscookings/assets/css/thm_reverscookings.css');

$session = JFactory::getSession();
$session->set('reverscookingsrezept', array());
$video = Thm_reverscookingsHelper::hasbild('#__thm_reverscookings_rezept_video', 'rezept_id', $this->item->id);
$image = Thm_reverscookingsHelper::hasbild('#__thm_reverscookings_rezept_bilder', 'rezept_id', $this->item->id);

?>
<script src="<?php echo JURI::root(true).'/components/com_thm_reverscookings/jwplayer/jwplayer.js'; ?>"  type="text/javascript" ></script>
<script type="text/javascript" >jwplayer.key="UWlvNkI/VkxwgyxZbuUTRkm1Afyo6RI6MupT1g=="</script>
<script type="text/javascript">


function deleteing (id){
	
	var temp = 'delete'+id;
	
	var rezeptid = document.getElementById(temp).value;


	var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.deleteing';

	 var data = 'ingid='+id+'&rezeptid='+rezeptid;
	 document.getElementById('content-ing').innerHTML="";
     var request = new Request({

     url: url,

     method:'get',

     data: data,

     onSuccess: function(responseText){

document.getElementById('content-ing').innerHTML=  responseText;

     }

     }).send();
}

function runButton() {
    
    var ingame = document.getElementById("ingname").value;

    document.getElementById("ingname").value="";

    var ingquantity = document.getElementById("ingquantity").value;
    
    document.getElementById("ingquantity").value="";
    

    var ingunit = document.getElementById("ingunit").value;

    document.getElementById("ingunit").value="";

    var ingid = document.getElementById("einfuegen").value;

    if(ingid == "einfuegen"){
        alert("Bitte nur Ingredient der Liste nehmen");
    	ingid =0;
    	 return;
    }
   
    if(ingame=="" || ingquantity=="" || ingunit=="")
        return;
		 tempquantity = parseInt(ingquantity);
		
	if(isNaN(tempquantity)){
        alert("Quantity muss ein zahl sein");
        return;
    	}
    
    
    var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.listFields';

    var data = 'ingid='+ingid+'&ingname='+ingame+'&ingquantity='+ingquantity+'&ingunit='+ingunit;

                        var request = new Request({

                        url: url,

                        method:'get',

                        data: data,

                        onSuccess: function(responseText){

document.getElementById('fields-container').innerHTML=  responseText;

                        }

                        }).send();

}

	function deleteKorb( id){
		var korbindex=id;
		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.deleteKorb';

		var data = 'korbindex='+korbindex;

		  var request = new Request({

              url: url,

              method:'get',

              data: data,

              onSuccess: function(responseText){

document.getElementById('fields-container').innerHTML=  responseText;

              }

              }).send();

		

	}
	
function autocom(){
		
		var teil = document.getElementById("ingname").value;

		if(teil.length >0){

		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.autocomplete';

		 var data = 'ingname='+teil;
		 
		  var request = new Request({

              url: url,

              method:'get',

              data: data,

              onSuccess: function(responseText){

document.getElementById('ing-container').innerHTML=  responseText;

              }

              }).send();
		}
		
	}
	
	function ausgewaehlt(ingid){
	   var ingredient = document.getElementById(ingid).value;
		var arr = ingredient.split(":");
		document.getElementById("ingname").value = arr[0];
		document.getElementById("ingunit").value = arr[1];
		document.getElementById("einfuegen").value = ingid;
		document.getElementById('ing-container').innerHTML='';
	}
	
	Joomla.submitbutton = function(task)
	{
		if (task == 'reverscookingsrezept.cancel' || document.formvalidator.isValid(document.id('reverscookingsrezept-form'))) {
			Joomla.submitform(task, document.getElementById('reverscookingsrezept-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
	 
</script>

<form action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="reverscookingsrezept-form" enctype="multipart/form-data" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_THM_REVERSCOOKINGS_LEGEND_REVERSCOOKINGSREZEPT'); ?></legend>
			<ul class="adminformlist">
                
				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>
				<li><?php echo $this->form->getLabel('created_by'); ?>
				<?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('namerezept'); ?>
				<?php echo $this->form->getInput('namerezept'); ?></li>
				<li><?php echo $this->form->getLabel('portionanzahl'); ?>
				<?php echo $this->form->getInput('portionanzahl'); ?></li>
				<li><?php echo $this->form->getLabel('ingredientname'); ?>
				<?php echo  Thm_reverscookingsModelreverscookingsrezept::getIngredient($this->item->id); ?></li>
				<li><?php echo $this->form->getLabel('zubereitung'); ?>
				<?php echo $this->form->getInput('zubereitung'); ?></li>
				<li><?php echo JText::_('VIEWS'); ?>:
			    <?php echo Thm_reverscookingsHelper::viewrezept($this->item->id) ?></li>
				<li><?php echo $this->form->getLabel('bewertung'); ?>
				<?php echo Thm_reverscookingsHelper::getRating($this->item->id); ?></li>
				<li><?php echo $this->form->getLabel('checked_out_time'); ?>
				<?php echo $this->form->getInput('checked_out_time'); ?></li>
				<li><?php echo $this->form->getLabel('zubereitungdauert'); ?>
				<?php echo $this->form->getInput('zubereitungdauert'); ?></li>
				<li><?php echo JText::_('BILD'); ?>:
				<?php if($image != null):?>
				<?php echo JHTML :: image('components/com_thm_reverscookings/img/rezept_bild/'.$image[0]->filename, $this->item->namerezept."_Bild", $this->attribs); ?><br>
				<?php endif;?>
				<input type="file" name = "image_upload" /></li>
				<li><?php echo JText::_('Video'); ?>:
				<?php if($video != null ):?>
				<div id='my-video'></div>
					<script type='text/javascript'>
   						 jwplayer('my-video').setup({
   						 'flashplayer' : 'player.swf',
       					 'file': "<?php echo JURI::root(true).'/components/com_thm_reverscookings/img/rezept_video/'. $video[0]->filename; ?>",
						 'image':"<?php echo JURI::root(true).'/components/com_thm_reverscookings/img/rezept_bild/'.$image[0]->filename; ?>",
        				 'width':'480',
       					 'height':'270'
   						 });
					</script>
					<?php endif;?>
				<input type="file" name = "video_upload"  /></li>
				<li><?php echo $this->form->getLabel('rezeptkategory'); ?>
				<?php echo $this->form->getInput('rezeptkategory'); ?></li>


            </ul>
            <h3><?php echo JText::_('COM_CODE141');?></h3>

	
            
		</fieldset>
		
	</div>


	<input type="hidden" name="task" value=""  />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>
<div  class="adminform">
		<?php echo Thm_reverscookingsModelReverscookingsrezept::getComments($this->item->id,$this->item->created_by);?>
	</div>
	
	<br/>
