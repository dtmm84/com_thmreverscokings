<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';

JHTML::_('behavior.mootools');

JHTML::_('behavior.framework', true);

/**
 * Reverscookingsrezept controller class.
 */
class Thm_reverscookingsControllerReverscookingsrezept extends JControllerForm
{

    function __construct() {
    	
    	self::saveingredient();
        $this->view_list = 'reverscookingsrezepts';
        parent::__construct();
       self:: savedate();
    }
    private function savedate(){
    	$post = JRequest::get( 'post' );
    	if(empty($post))
    		return;
    	
    	if($post['task']=='save'||$post['task']=='apply'||$post['task']=='save2new'||$post['task']=='save2copy'){
    		$rid = $post['jform']['id'];
    		var_dump($rid);
    		$datatime = $post['jform']['checked_out_time'];
    		$user_id = $post['jform']['created_by'];
    	if($rid == 0){
    		$maxid =Thm_reverscookingsHelper::maxid('#__thm_reverscookings_rezept');
    		$rid = $maxid[0]->id +1 ;
    	}
    	Thm_reverscookingsHelper::updatetime($rid, $datetime, '#__thm_reverscookings_rezept') ;
    	Thm_reverscookingsHelper::savebild($user_id, $rid,true);
    	Thm_reverscookingsHelper::savevideo($user_id, $rid);
    	}
    }
    
    function saveingredient (){
    	$post = JRequest::get( 'post' );
    	if(empty($post))
    		return;
    
    	if($post['task']=='save'||$post['task']=='apply'||$post['task']=='save2new'||$post['task']=='save2copy'){
    		$db = JFactory::getDBO();
    		$session =& JFactory::getSession();
    		$ingkorb = $session->get('reverscookingsrezept');
    		$rezeptingredient = $post['ingredient'];
    		$rezeptid = $post['jform']['id'];
    		if(!empty($rezeptingredient)){
    			foreach ($rezeptingredient as $index => $value){
    				$quantity = intval($value);
    				$query = 'UPDATE #__thm_reverscookings_ingredients_rezept  SET quantity ='.$quantity.' WHERE id ='.$index;
    				$db->setQuery($query);
    				$db->query();
    				$db->loadResult();
    			}
    		}
    		if(!empty($ingkorb)){
    			if($rezeptid ==0){
    				$id = Thm_reverscookingsHelper::maxid('#__thm_reverscookings_rezept');
    				var_dump($id);
    				$rezeptid = $id[0]->id + 1;
    				var_dump($rezeptid);
    					
    			}
    			foreach ($ingkorb as $element ){
    				$indexs= $element->id ;
    				$ingvalue= $element->value;
    				if($indexs !=0 && !empty($ingvalue[1])){
    					$query = "INSERT #__thm_reverscookings_ingredients_rezept(ingid,rezeptid,quantity)
    					VALUES ('$indexs' , '$rezeptid' , '$ingvalue[1]')";
    					$db->setQuery($query);
    					$db->query();
    				}
    
    			}
    		}
    	}
    }
    
    function  deletecomment(){
    	$post = JRequest::get('post');
    	if(empty($post))
    		return;
    	$receptId= intval( $post['rezeptid']);
    	$userId= intval($post['user_id']);
    	$commentId= intval($post['commentid']);
    	$akuser = JFactory::getUser();
    	$db = JFactory::getDbo();
    	if(isset($akuser->groups[8]) || isset($akuser->groups[7])){
    	$query = 'DELETE FROM #__thm_reverscookings_comment_rezept  WHERE id ='.$commentId;
    	
    	$db->setQuery($query);
    	$db->query();
    	}
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&layout=edit&id='.$receptId, false));
    	
    }
      
    

}