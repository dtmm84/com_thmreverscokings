<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>  
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');



/**
 * Reverscookingsrezepts list controller class.
 */
class Thm_reverscookingsControllerReverscookingsrezepts extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 */
	public function getModel($name = 'reverscookingsrezept', $prefix = 'Thm_reverscookingsModel')
	{
		self::deletepart();
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		
		return $model;
	}
    
    
	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 */
	public function saveOrderAjax()
	{
		// Get the input
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);
		
		$z=$_POST+2;
		

		if ($return)
		{
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}
    
    
	public  function deletepart(){
    	$post = JRequest::get( 'post' );
    	$db = JFactory::getDBO();
    	if($post['task']=='delete'){
    		$checked = $post['boxchecked'];
    		$ids = $post['cid'];
    		for($i =0; $i<$checked ;$i++){
    			$query = "DELETE FROM #__thm_reverscookings_ingredients_rezept WHERE rezeptid=".$ids[$i];
    			$db->setQuery($query);
    			$db->query();
    			$query = "DELETE FROM #__thm_reverscookings_comment_rezept WHERE rezeptid=".$ids[$i];
    			$db->setQuery($query);
    			$db->query();
    			$query = "DELETE FROM #__thm_reverscookings_bewertung_rezept WHERE rezeptid=".$ids[$i];
    			$db->setQuery($query);
    			$db->query();
    			Thm_reverscookingsHelper::deletedata($ids[$i], "rezept_id", "#__thm_reverscookings_rezept_bilder");
    			Thm_reverscookingsHelper::deletedata($ids[$i], "rezept_id", "#__thm_reverscookings_rezept_video ");
    		}
    	}
    	
    }
    
}