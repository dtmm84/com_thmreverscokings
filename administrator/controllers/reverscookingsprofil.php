<?php
/**
 * @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';


/**
 * Reverscookingsprofil controller class.
 */
class Thm_reverscookingsControllerReverscookingsprofil extends JControllerForm
{

    function __construct() {
        $this->view_list = 'reverscookingsprofils';
        parent::__construct();
        self::savedate();
    }
    
    private function savedate(){
    	$post = JRequest::get( 'post' );
    	if(empty($post))
    		return;
    	 
    	if($post['task']=='save'||$post['task']=='apply'||$post['task']=='save2new'||$post['task']=='save2copy'){
    		$pid = $post['jform']['id'];
    		$datatime = $post['jform']['checked_out_time'];
    		$user_id = $post['jform']['created_by'];
    		if($pid == 0){
    			$maxid =Thm_reverscookingsHelper::maxid('#__thm_reverscookingsprofils');
    			$pid = $maxid[0]->id +1 ;
    		}
    		Thm_reverscookingsHelper::updatetime($pid, $datetime, '#__thm_reverscookingsprofils') ;
    		Thm_reverscookingsHelper::savebild($user_id, $pid,false);
    	}
    }

}