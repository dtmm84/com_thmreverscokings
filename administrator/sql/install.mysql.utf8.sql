CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_ingredients` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL  ,
`checked_out_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`created_by` INT(11)  NOT NULL ,
`ingname` VARCHAR(255)  NOT NULL ,
`ingunit` VARCHAR(255)  NOT NULL ,
`ingkategory` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8_general_ci;


CREATE TABLE IF NOT EXISTS `#__thm_reverscookingsprofils` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL  ,
`checked_out_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`created_by` INT(11)  NOT NULL ,
`uname` VARCHAR(255)  NOT NULL ,
`address` VARCHAR(255)  NOT NULL ,
`plz` INT(11)  NOT NULL ,
`ort` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`bild` TEXT NOT NULL ,
`virtualfridge` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_rezept` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `ordering` int(11) NOT NULL,
 `state` tinyint(1) NOT NULL DEFAULT '1',
 `checked_out` int(11) NOT NULL,
 `checked_out_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `created_by` int(11) NOT NULL,
 `namerezept` varchar(255) NOT NULL,
 `zubereitung` text NOT NULL,
 `views` int(11) NOT NULL DEFAULT '1',
 `zubereitungdauert` int(11) NOT NULL,
 `bild` text NOT NULL,
 `rezeptkategory` int(11) NOT NULL,
 `portionanzahl` int(11) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_ingredients_rezept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingid` int(11) NOT NULL,
  `rezeptid` int(11) NOT NULL,
  `quantity` dec(11,2) NOT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Übergangstabelle für Ingredient und Zutaten' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_comment_rezept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `rezeptid` int(11) NOT NULL,
  `comment`  TEXT NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Übergangstabelle für Comment und Rezept' AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_bewertung_rezept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `rezeptid` int(11) NOT NULL,
  `bewertung`  FLOAT(11,2) NOT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Übergangstabelle für Bewertung und Rezept' AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_virtual_fridge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `ingid` int(11) NOT NULL,
  `quantity`  dec(11,2) NOT NULL,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Übergangstabelle für User und Zutaten' AUTO_INCREMENT=1;



-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `#__thm_reverscookings_rezept_bilder`
--

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_rezept_bilder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rezept_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `#_thm_reverscookings_rezept_video`
--

CREATE TABLE IF NOT EXISTS `#__thm_reverscookings_rezept_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rezept_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


--
-- Tabellenstruktur für Tabelle `#__thm_reverscookingsprofils_bild`
--

CREATE TABLE IF NOT EXISTS `#__thm_reverscookingsprofils_bild` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profilid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;
