<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');


abstract class Thm_reverscookingsHelper
{
	
	//Such die Rezept für ein Benutzer
	public static function userRezepts($userid , $sort){
		if(empty($sort))
			$sort="namerezept";
		$result = " ";
		$db = JFactory::getDBO();
		$query ='SELECT * FROM #__thm_reverscookings_rezept WHERE created_by ='.$userid.' ORDER BY '.$sort;
		$db->setQuery($query);
		$db->query();
		$rezepts = $db->loadObjectList();
		if(!empty($rezepts)){
			foreach ($rezepts as $rez){
				$result.="<tr><td><a href=".JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id='.$rez->id).">"
				.$rez->namerezept."</a></td><td><a>".$rez->zubereitungdauert."</a></td><td>".$rez->views."</td><td>".$rez->checked_out_time."</td>";
				if($rez->state == 1)
				$result.= "<td><a>".JText::_('COM_CODE135')."</a></td>";
				else{
					$result.="<td><a>".JText::_('COM_CODE136')."</a></td>";
				}
				$result.='<td><form  action='.JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.delete&id='.$rez->id).
				' ><input type="submit" value="'.JText::_('COM_CODE121').'"></form></td></tr>';
				}
		}
		
		return $result;
	}
	
	//give the max id of recpet Table
	public static function maxid($datenbanktabel){
		$db = JFactory::getDBO();
		$ingredientquery='SELECT MAX(a.id) AS id FROM '.$datenbanktabel.' AS a' ;
		$db->setQuery($ingredientquery);
		$db->query();
		return  $db->loadObjectList();
	}
	//search a ingredient name in the Database
	public static function  inglikename($teilname){
		$temp = '%'.$teilname.'%';
		$db = JFactory::getDBO();
		$ingredientquery="SELECT * FROM `#__thm_reverscookings_ingredients` WHERE ingname LIKE '$temp'";
		$db->setQuery($ingredientquery);
		$db->query();
		return  $db->loadObjectList();
	}
	//Delete an ingredient of a Rezept
	public static function  deleteingrezept($id){
		$db = JFactory::getDBO();
		$deletingrezept = 'DELETE FROM #__thm_reverscookings_ingredients_rezept  WHERE id='.$id;
		$db->setQuery($deletingrezept);
		$db->query();
		return true;
	}
	//Delete a ingredient in the Virtualfridege
	public static function  deletevirtualfridge($id){
		$db = JFactory::getDBO();
		$deletingrezept = 'DELETE FROM #__thm_reverscookings_virtual_fridge  WHERE id='.$id;
		$db->setQuery($deletingrezept);
		$db->query();
		return true;
	}
	// Insert a Ingredient  in the Virtualfridge
	public static function  inserVirtualfridge($profilid, $ingkorb){
		$db = JFactory::getDBO();
		foreach ($ingkorb as $element ){
			$indexs= $element->id ;
			$ingvalue= $element->value;
			if($indexs !=0 && !empty($ingvalue[1])){
				$query = "INSERT #__thm_reverscookings_virtual_fridge(ingid,userid,quantity)
				VALUES ('$indexs' , '$profilid' , '$ingvalue[1]')";
				$db->setQuery($query);
				$db->query();
			}
			 
		}
	}
	//view for the Ingredientlist of a Recept
	public static function newinglist($rezeptid){
		$db = JFactory::getDBO();
		$result='<table ><tr><th>Name</th><th>quantity</th><th>unit</th><th>Option</th></tr>';
	
		$query = 'SELECT rezeptingredient.id AS id, ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, rezeptingredient.quantity AS ingquantity
		FROM  #__thm_reverscookings_ingredients_rezept AS rezeptingredient LEFT JOIN  #__thm_reverscookings_rezept AS rezepts
		ON rezeptingredient.rezeptid = rezepts.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON rezeptingredient.ingid = ingredients.id WHERE rezepts.id ='.$rezeptid;
		$db->setQuery($query);
		$db->query();
		$ingredients = $db->loadObjectList();
		if(!empty($ingredients)){
			foreach ($ingredients as $ing){
				$del= 'delete'.$ing->id;
				$result .='<tr><td>'.$ing->ingname.'</td><td><input type="text" id="quantity"  name= ingredient['.$ing->id.']'.' value='.$ing->ingquantity.' /></td>
				<td>'.$ing->ingunit.'</td><td><button  id='.$del.'  value='.$rezeptid.' onclick="deleteing('.$ing->id.')">delete</button></td></tr>';
			}
		}
		$result .='</table>';
		return $result;
	}
	// view for the Ingredientliste of a User
	public static function newinguserlist($profilid){
		$db = JFactory::getDBO();
		
		$result='<table ><tr><th>Name</th><th>quantity</th><th>unit</th><th>Option</th></tr>';
	
		$query = 'SELECT useringredient.id AS id, ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, useringredient.quantity AS ingquantity
		FROM  #__thm_reverscookings_virtual_fridge AS useringredient LEFT JOIN  #__thm_reverscookingsprofils AS profils
		ON useringredient.userid = profils.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON useringredient.ingid = ingredients.id WHERE profils.id ='.$profilid;
		$db->setQuery($query);
		$db->query();
		$ingredients = $db->loadObjectList();
		if(!empty($ingredients)){
			foreach ($ingredients as $ing){
				$del= 'delete'.$ing->id;
				$result .='<tr><td>'.$ing->ingname.'</td><td><input type="text" id="quantity"  name= ingredient['.$ing->id.']'.' value='.$ing->ingquantity.' /></td>
				<td>'.$ing->ingunit.'</td><td><button  id='.$del.'  value='.$profilid.' onclick="deleteing('.$ing->id.')">delete</button></td></tr>';
			}
		}
		$result .='</table>';
		return $result;
	}
	//Calculate the view for a Rezept
	public static function viewrezept($rezeptid){
		if($rezeptid == 0)
			return;
		$db =& JFactory::getDbo();
		$query='SELECT views as value FROM #__thm_reverscookings_rezept WHERE id='.$rezeptid;
		$db->setQuery($query);
		$db->query();
		$view = $db->loadObjectList();
		$intview= intval($view[0]->value);
		$result = $intview + 1;
		$update='UPDATE #__thm_reverscookings_rezept  SET views ='.$result.' WHERE id='.$rezeptid;
		$db->setQuery($update);
		$db->query();
		return $result;
	}
	//Update the time for a Rezept in Frontend
	public static function updatetime($id, $datetime,$tabel){
		$db =& JFactory::getDbo();
		$update="UPDATE ".$tabel."  SET checked_out_time ='$datetime'  WHERE id=".$id;
		$db->setQuery($update);
		$db->query();
		
	}
	//Seachr Ingredient for a Rezept
public static function getRecipePerIng($ingredients = null,$inganzahl) {
	
	
		if($ingredients != NULL) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('r.*' );
			$query->select('count(ir.id) AS treffer ');
			$query->from('#__thm_reverscookings_rezept AS r');
	
	
			$query->leftJoin('#__thm_reverscookings_ingredients_rezept AS ir ON r.id = ir.rezeptid');
			$query->leftJoin('#__thm_reverscookings_ingredients AS i ON ir.ingid = i.id');
	
			$query->where($ingredients);
			$query->group('r.id');
			$query->order('treffer DESC');
			
			$db->setQuery($query);
			$rezepts = $db->loadObjectList();
		}
		$result ='<div><table width="100%" cellspacing="20" cellpadding="20"><tr><th></th><th></th></tr>';
		
		foreach ($rezepts as $rezt){
			$rating = self::getRating($rezt->id);
			$rezing = self::getInganzahl($rezt->id);
			$result .='<tr><td width="40%"><a href= '.JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id='. $rezt->id).
			'><b>'.$rezt->namerezept.'</b></a></td><td width="50%"> '.JText::_('COM_CODE110').' <b style="color:red;">'.$rezt->treffer.'</b>/'.$rezing.$rezIng.JText::_('COM_CODE111').$rezt->views.JText::_('COM_CODE112').$rating.
			'</td></tr>';
		}
		$result.='</table></div>';
		return $result;
	}
	//check , if the User has a profil
	public static  function hasprofil($userid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from(' #__thm_reverscookingsprofils');
		$query->where('created_by ='.$userid);
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if(empty($result))
			return false;
		return $result;
	}
	//check, if this Profil is for this User for the Editing
	public static  function isMyprofil($userid, $profilid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('created_by');
		$query->from(' #__thm_reverscookingsprofils');
		$query->where('id  ='.$profilid);
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if($result[0]->created_by == $userid )
			return true;
		return false;
	}
	//check, if this Rezept is for this User for the Editing
	public static  function isMyrezept($userid, $rezeptid){
		if(empty($rezeptid))
			return false;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('created_by');
		$query->from(' #__thm_reverscookings_rezept');
		$query->where(' id  ='.$rezeptid);
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if($result[0]->created_by == $userid )
			return true;
		return false;
	}
	
	public static function hasbild($datenbanktabel,$element, $element_value){
		$db = JFactory::getDbo();
		$query = "SELECT * FROM ".$datenbanktabel." WHERE ".$element." = '$element_value'";
		$db->setQuery($query);
		$db->query();
		$result = $db->loadObjectList();
		if(empty($result))
			return false;
		return $result;
		
	}
	// Save the Image for a Model
	public static function savebild($user_id, $element_value, $isrezept){
		$file = JRequest::getVar('image_upload', null, 'files', 'array');
		$db = JFactory::getDbo();
		$filename = JFile::makeSafe($file['name']);
		$filesize = $file['size'] /1024;
		
		
		if($filesize <1)
			return;
		$src = $file['tmp_name'];
		$format = JFile::getExt($filename);
		$newfilename = $element_value.'_'.$user_id.'.'.$format;
		$dest = JPATH_COMPONENT . DS . "img" . DS ."profil_bild".DS.$newfilename;
		$datenbanktabel = '#__thm_reverscookingsprofils_bild';
		$element = 'profilid';
		if($isrezept == true){
			$dest = JPATH_COMPONENT . DS . "img" . DS ."rezept_bild".DS.$newfilename;
			$datenbanktabel = '#__thm_reverscookings_rezept_bilder';
			$element = 'rezept_id';
		}
		unlink($dest);
		if ( $format== 'jpg' || $format == 'gif' ||$format =='png' || $format =='jpeg' || $format =='swf') {
			if ( JFile::upload($src, $dest) ) {
				$query ='';
			if(	self::hasbild($datenbanktabel, $element, $element_value) == false){
				$query="INSERT ".$datenbanktabel." (".$element.",user_id , size , format , filename)
    					VALUES ('$element_value' , '$user_id' , '$filesize',' $format','$newfilename')";
			}
			else{
				$query = "UPDATE ".$datenbanktabel."  
							SET size  ='$filesize' , format ='$format' , filename = '$newfilename' WHERE ".$element." =".$element_value;
			}
			$db->setQuery($query);
			$db->query();
				return true;
			}
		} else {
			return "NF";
		}
	}
	//Save the Video for a Rezept
	public static function savevideo($user_id, $element_value){
		$file = JRequest::getVar('video_upload', null, 'files', 'array');
		$db = JFactory::getDbo();
		$filename = JFile::makeSafe($file['name']);
		$filesize = $file['size'] /(1024*2);
		$src = $file['tmp_name'];
		$format = JFile::getExt($filename);
		$newfilename = $element_value.'_'.$user_id.'.'.$format;
		$dest = JPATH_COMPONENT . DS . "img" . DS ."rezept_video".DS.$newfilename;
		$datenbanktabel = '#__thm_reverscookings_rezept_video';
		$element = 'rezept_id';
		
		unlink($dest);
		if ( $format== 'flv' || $format == 'mp4'||$format == 'wmv'  ) {
			if ( JFile::upload($src, $dest) ) {
				$query ='';
				if(	self::hasbild($datenbanktabel, $element, $element_value) == false){
					$query="INSERT ".$datenbanktabel." (".$element.",user_id , size , format , filename)
					VALUES ('$element_value' , '$user_id' , '$filesize',' $format','$newfilename')";
				}
				else{
					$query = "UPDATE ".$datenbanktabel."
					SET size  ='$filesize' , format ='$format' , filename = '$newfilename' WHERE ".$element." =".$element_value;
				}
				$db->setQuery($query);
				$db->query();
				return true;
			}
		} else {
			return "NF";
		}
	}
	// delete Image oder Video
	public static function deletedata($elementId,$attribut, $basetable){
		$db = JFactory::getDbo();
		$query = "SELECT filename FROM ".$basetable." WHERE ".$atribut." =".$elementId;
		$db->setQuery($query);
		$data = $db->loadObjectList();
		$filename = $data[0]->filename;
		unlink($filename);
		$query = "DELETE FROM ".$basetable." WHERE ".$attribut." = ".$elementId;
		$db->setQuery($query);
		$db->query();
	}
	public static function getRating($id){
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = 'SELECT AVG(bewertung) AS bewertung
		FROM  #__thm_reverscookings_bewertung_rezept
		WHERE rezeptid = '.$id;
		$db->setQuery($query);
		$ratings = $db->loadObjectList();
		if(!empty($ratings)){
			foreach ($ratings as $rating){
				$result = $rating->bewertung;
			}
		}
	
		$result .= '</div>';
		return round($result,2);
	}
	//Zeigt die Profil an
	public static function myprofil($userid, $element){
		$attribs=array();
		$attribs['width'] = '95px';
		$attribs['height'] = '50px';
		$db = JFactory::getDbo();
		$query = 'SELECT * FROM  #__thm_reverscookingsprofils WHERE '.$element.' ='.$userid;
		$db->setQuery($query);
		$userdata = $db->loadObjectList();
		$image = self::hasbild('#__thm_reverscookingsprofils_bild', 'profilid', $userdata[0]->id);
		$result='<div>';
		$result .= '<div><h4><a href="'.JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&id='.$userdata[0]->id).'">'.$userdata[0]->uname.'</a></h4></div>';
		$result .= '<div>'.JHTML :: image('components/com_thm_reverscookings/img/profil_bild/'.$image[0]->filename, $userdata[0]->uname."_Bild", $attribs).'</div>';
		$result .='</div>';
		return $result;
	
	}
	//Gibt die Zutatenanzahl von einer Rezpt zurück
	public function getInganzahl($rezid){
		$db = JFactory::getDbo();
		$query= 'SELECT count(ingid) as zahl FROM  #__thm_reverscookings_ingredients_rezept WHERE  rezeptid='.$rezid;
		$db->setQuery($query);
		$a = $db->loadObjectList();
		return $a[0]->zahl;
	
	}
}

