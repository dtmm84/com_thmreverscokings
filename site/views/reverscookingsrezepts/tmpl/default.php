<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 * @author Bassing @author Bassing <dominik.bassing@mni.thm.de>
 * @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 * @author Schneider <stefan.schneider@mni.thm.de>
 * @author Omoko <guy.bertrand.omoko@hotmail.com>
 * @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>> -
 */


// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script','system/multiselect.js',false,true);

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_thm_reverscookings');
$saveOrder	= $listOrder == 'a.ordering';
?>

<form
	action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezepts'); ?>"
	method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
			</label> <input type="text" name="filter_search" id="filter_search"
				value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
				title="<?php echo JText::_('Search'); ?>" />
			<button type="submit">
				<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
			</button>
			<button type="button"
				onclick="document.id('filter_search').value='';this.form.submit();">
				<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
			</button>
		</div>

		<div class='filter-select fltrt'>
			<?php //Filter for the field rezeptkategory
$selected_rezeptkategory = JRequest::getVar('filter_rezeptkategory');
jimport('joomla.form.form');
JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
$form = JForm::getInstance('com_thm_reverscookings.reverscookingsrezept', 'reverscookingsrezept');
echo JText::_('COM_REVERSCOOKING_RECIPEKAT').':' ;
echo $form->getInput('filter_rezeptkategory', null, $selected_rezeptkategory);
?>
		</div>


	</fieldset>
	<div class="clr"></div>

	<table class="adminlist">
		<thead>
			<tr>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'COM_REVERSCOOKING_RECIPENAME', 'a.namerezept', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'COM_REVERSCOOKING_RECIPEKAT', 'a.rezeptkategory', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'COM_REVERSCOOKING_RECIPETIME', 'a.zubereitungdauert', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'COM_REVERSCOOKING_RECIPEVIEWS', 'a.views', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JText::_('BEWERTUNG'); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo  JText::_('BEWERTUNGEN'); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'COM_REVERSCOOKING_RECIPE_CHECKED', 'a.checked_out_time', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="10"><?php echo $this->pagination->getLimitBox(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($this->items as $item) : ?>
			<tr>
				<td><a
					href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id=' . (int)$item->id); ?>"><?php echo $item->namerezept; ?>
				
				</a>&nbsp;&nbsp;</td>
				<td><?php echo $this->escape($item->rezeptkategory); ?>
				</td>
				<td><?php echo $this->escape($item->zubereitungdauert); ?>
				</td>
				<td><?php echo $this->escape($item->views); ?>
				</td>
				<td><?php echo Thm_reverscookingsModelReverscookingsrezepts::getRating($this->escape($item->id));?>
				</td>
				<td><?php echo Thm_reverscookingsModelReverscookingsrezepts::getRatingcount($this->escape($item->id));?>
				</td>
				<td> <?php echo $this->escape($item->checked_out_time);?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
	    <input type="hidden" name="boxchecked" value="0" /> 
	    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" /> 
	    <input 	type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<div class="pagination">
<?php echo $this->pagination->getPagesLinks(); ?>
<p class="counter">
<?php echo $this->pagination->getPagesCounter(); ?>
</p>
</div>
