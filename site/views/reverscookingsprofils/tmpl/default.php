<?php
/**
 * @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */


// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script','system/multiselect.js',false,true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_thm_reverscookings/assets/css/thm_reverscookings.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_thm_reverscookings');
$saveOrder	= $listOrder == 'a.ordering';
?>

<form action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofils'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('Search'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>
	<table class="adminlist">
		<thead>
			<tr>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'Username', 'a.uname', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'Adresse', 'a.address', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'PLZ', 'a.plz', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'Ort', 'a.ort', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
				<th style="background: #ccc;"><?php echo JHtml::_('grid.sort', 'E-Mail', 'a.email', $listDirn, $listOrder); ?>&nbsp;&nbsp;
				</th>
			</tr>
		</thead>
		
		<tbody>
		<?php foreach ($this->items as $i => $item) : ?>
			<tr>
				<td><a
					href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&id=' . (int)$item->id); ?>"><?php echo $item->uname; ?></a>&nbsp;&nbsp;</td>
				<td><?php echo $this->escape($item->address); ?>&nbsp;&nbsp;
				</td>
				<td><?php echo $this->escape($item->plz); ?>&nbsp;&nbsp;
				</td>
				<td><?php echo $this->escape($item->ort); ?>&nbsp;&nbsp;
				</td>
				<td><?php echo $this->escape($item->email); ?>&nbsp;&nbsp;
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<div class="pagination">
<?php echo $this->pagination->getPagesLinks(); ?>
<p class="counter">
<?php echo $this->pagination->getPagesCounter(); ?>
</p>
</div>