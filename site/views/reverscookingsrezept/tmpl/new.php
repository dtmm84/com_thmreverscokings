<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_thm_reverscookings', JPATH_ADMINISTRATOR);


$session =& JFactory::getSession();
$session->set('reverscookingsrezept', array());
$dat= getdate();
?>
<script src="<?php echo JURI::root(true).'/components/com_thm_reverscookings/assets/rezept/new.js'; ?>"  type="text/javascript" ></script>


<?php if(!$this->isguest):?>
<div class="reverscookingsrezept-edit front-end-edit">
  

    <form id="form-reverscookingsrezept" action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <table width=600>		
			<tr>
				<td><?php echo $this->form->getLabel('namerezept'); ?></td>
				<td><?php echo $this->form->getInput('namerezept'); ?></td>
			</tr>
			<tr>
				<td><?php echo $this->form->getLabel('portionanzahl'); ?></td>
				<td><?php echo $this->form->getInput('portionanzahl'); ?></td>
			</tr>
			<tr>	
				<td><?php echo $this->form->getLabel('zubereitungdauert'); ?></td>
				<td><?php echo $this->form->getInput('zubereitungdauert'); ?></td>
			</tr>
			 <td><?php echo $this->form->getInput('created_by'); ?></td>
			
			
		
				<td><?php echo $this->form->getInput('checked_out_time'); ?></td>
			<tr>
				<td><?php echo JText::_('BILD'); ?></td>
				<td><input type="file" name = "image_upload" /></td>
			</tr>
			<tr>
				<td><?php echo JText::_('Video'); ?></td>
				<td><input type="file" name = "video_upload"  /></td>
			</tr>
			<tr>
				<td><?php echo $this->form->getLabel('rezeptkategory'); ?></td>
				<td><?php echo $this->form->getInput('rezeptkategory'); ?></td>
			</tr>
		</table>
		
		
		<br/><?php echo $this->form->getLabel('ingredientname'); ?>
			 <?php echo  Thm_reverscookingsModelreverscookingsrezept::getIngredient(0); ?>
			 
		<br/><?php echo $this->form->getLabel('zubereitung'); ?>
			 <?php echo $this->form->getInput('zubereitung'); ?>
		
		<br/><br/>
		<table width=1000>
        	<tr>
        		<td><?php echo $this->form->getLabel('state'); ?></td>
				<td><?php echo $this->form->getInput('state'); ?></td>
			</tr>
		</table>
		<div>
		<br/>
			<button type="submit" class="validate"><span><?php echo JText::_('SAVE'); ?></span></button>
			<input type="hidden" name="option" value="com_thm_reverscookings" />
			<input type="hidden" name="task" value="reverscookingsrezept.save" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
<?php else :?>
<H2>Only for Registry User</H2>
<?php endif;?>
