<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_thm_reverscookings', JPATH_ADMINISTRATOR);

$session =& JFactory::getSession();
$session->set('reverscookingsrezept', array());
$dat= getdate();
$video = Thm_reverscookingsHelper::hasbild('#__thm_reverscookings_rezept_video', 'rezept_id', $this->item->id);
$image = Thm_reverscookingsHelper::hasbild('#__thm_reverscookings_rezept_bilder', 'rezept_id', $this->item->id);
?>
<script src="<?php echo JURI::root(true).'/components/com_thm_reverscookings/jwplayer/jwplayer.js'; ?>"  type="text/javascript" ></script>
<script type="text/javascript" >jwplayer.key="UWlvNkI/VkxwgyxZbuUTRkm1Afyo6RI6MupT1g=="</script>
<script type="text/javascript">


function deleteing (id){
	
	var temp = 'delete'+id;
	
	var rezeptid = document.getElementById(temp).value;


	var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.deleteing';

	 var data = 'ingid='+id+'&rezeptid='+rezeptid;
	 document.getElementById('content-ing').innerHTML="";
     var request = new Request({

     url: url,

     method:'get',

     data: data,

     onSuccess: function(responseText){

document.getElementById('content-ing').innerHTML=  responseText;

     }

     }).send();
}

function runButton() {
    
    var ingame = document.getElementById("ingname").value;

    document.getElementById("ingname").value="";

    var ingquantity = document.getElementById("ingquantity").value;
    
    document.getElementById("ingquantity").value="";
    

    var ingunit = document.getElementById("ingunit").value;

    document.getElementById("ingunit").value="";

    var ingid = document.getElementById("einfuegen").value;

    if(ingid == "einfuegen"){
        alert("Bitte nur Ingredient der Liste nehmen");
    	ingid =0;
    	 return;
    }
   
    if(ingame=="" || ingquantity=="" || ingunit==""){
        alert("keine leere Feld ..Bitte!!");
        return;
    }
		 tempquantity = parseInt(ingquantity);
		
	if(isNaN(tempquantity)){
        alert("Quantity muss ein zahl sein");
        return;
    	}
    
    
    var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.listFields';

    var data = 'ingid='+ingid+'&ingname='+ingame+'&ingquantity='+ingquantity+'&ingunit='+ingunit;

                        var request = new Request({

                        url: url,

                        method:'get',

                        data: data,

                        onSuccess: function(responseText){

document.getElementById('fields-container').innerHTML=  responseText;

                        }

                        }).send();

}

	function deleteKorb( id){
		var korbindex=id;
		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.deleteKorb';

		var data = 'korbindex='+korbindex;

		  var request = new Request({

              url: url,

              method:'get',

              data: data,

              onSuccess: function(responseText){

document.getElementById('fields-container').innerHTML=  responseText;

              }

              }).send();

		

	}
	
function autocom(){
		
		var teil = document.getElementById("ingname").value;

		if(teil.length >0){

		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsrezept.autocomplete';

		 var data = 'ingname='+teil;
		 
		  var request = new Request({

              url: url,

              method:'get',

              data: data,

              onSuccess: function(responseText){

document.getElementById('ing-container').innerHTML=  responseText;

              }

              }).send();
		}
		
	}
	
	function ausgewaehlt(ingid){
	   var ingredient = document.getElementById(ingid).value;
		var arr = ingredient.split(":");
		document.getElementById("ingname").value = arr[0];
		document.getElementById("ingunit").value = arr[1];
		document.getElementById("einfuegen").value = ingid;
		document.getElementById('ing-container').innerHTML='';
	}
	
	 
</script>

<style>
    .front-end-edit ul {
        padding: 0 ;
    }
    .front-end-edit li {
        list-style: none;
        margin-bottom: 6px;
    }
    .front-end-edit label {
        margin-right: 10px;
        display: block;
        float: left;
        width: 200px ;
    }
    .front-end-edit .radio label {
        display: inline;
        float: none;
    }
    .front-end-edit .readonly {
        border: none ;
        color: #666;
    }    
    .front-end-edit #editor-xtd-buttons {
        height: 50px;
        width: 600px;
        float: left;
    }
    .front-end-edit .toggle-editor {
        height: 50px;
        width: 120px;
        float: right;
        
    }
</style>
<?php if($this->ismyrezept || $this->item->id ==0):?>
<div class="reverscookingsrezept-edit front-end-edit">
    <h1>Edit <?php echo $this->item->id; ?></h1>

    <form id="form-reverscookingsrezept" action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <ul>
				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>
				<li><?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('namerezept'); ?>
				<?php echo $this->form->getInput('namerezept'); ?></li>
				<li><?php echo $this->form->getLabel('portionanzahl'); ?>
				<?php echo $this->form->getInput('portionanzahl'); ?></li>
				<li><?php echo $this->form->getInput('checked_out_time'); ?></li>
				<li><?php echo JText::_('BILD'); ?>:
				<?php if($image != null):?>
				<?php echo JHTML :: image('components/com_thm_reverscookings/img/rezept_bild/'.$image[0]->filename, $this->item->namerezept."_Bild", $this->attribs); ?><br>
				<?php endif;?>
				<input type="file" name = "image_upload" /></li>
				<li><?php echo JText::_('Video'); ?>:
				<?php if($video != null ):?>
				<div id='my-video'></div>
					<script type='text/javascript'>
   						 jwplayer('my-video').setup({
   						 'flashplayer' : 'player.swf',
       					 'file': "<?php echo JURI::root(true).'/components/com_thm_reverscookings/img/rezept_video/'. $video[0]->filename; ?>",
						 'image':"<?php echo JURI::root(true).'/components/com_thm_reverscookings/img/rezept_bild/'.$image[0]->filename; ?>",
        				 'width':'480',
       					 'height':'270'
   						 });
					</script>
					<?php endif;?>
				<input type="file" name = "video_upload"  /></li>
				<li><?php echo $this->form->getLabel('rezeptkategory'); ?>
				<?php echo $this->form->getInput('rezeptkategory'); ?></li>
				<li><?php echo $this->form->getLabel('ingredientname'); ?>
				<?php echo  Thm_reverscookingsModelreverscookingsrezept::getIngredient($this->item->id); ?></li>
				<li><?php echo $this->form->getLabel('zubereitung'); ?>
				<?php echo $this->form->getInput('zubereitung'); ?></li>
				<li><?php echo JText::_('VIEWS'); ?>:
			<?php echo Thm_reverscookingsHelper::viewrezept($this->item->id) ?></li>
				<li><?php echo $this->form->getLabel('zubereitungdauert'); ?>
				<?php echo $this->form->getInput('zubereitungdauert'); ?></li>
				

        </ul>
		<div>
			<button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
			<?php echo JText::_('or'); ?>
			<a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>

			<input type="hidden" name="option" value="com_thm_reverscookings" />
			<input type="hidden" name="task" value="reverscookingsrezept.save" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
<?php else :?>
<H2>That is not your Rezept</H2>
<?php endif;?>
