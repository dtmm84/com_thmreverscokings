<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_thm_reverscookings', JPATH_ADMINISTRATOR);

$dat= getdate();
$attribs= array();

$video = Thm_reverscookingsHelper::hasbild('#__thm_reverscookings_rezept_video', 'rezept_id', $this->item->id);
$image = Thm_reverscookingsHelper::hasbild('#__thm_reverscookings_rezept_bilder', 'rezept_id', $this->item->id);
$videoformat = $video[0]->format;

$user = & JFactory::getUser();
?>
 
<script src="<?php echo JURI::root(true).'/components/com_thm_reverscookings/jwplayer/jwplayer.js'; ?>"  type="text/javascript" ></script>
<script type="text/javascript" >jwplayer.key="UWlvNkI/VkxwgyxZbuUTRkm1Afyo6RI6MupT1g=="</script>
<?php if( $this->item ) : ?>
 <?php if($this->ismyrezept || $this->item->id ==0): ?>
		<form action="input_button.htm">
			<p>
				<input type="button" name="button" value="<?php echo JText::_('EDIT'); ?>" onClick="window.location='<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.edit&id='.$this->item->id); ?>'">
			</p>
		</form>
	<?php endif; ?>

    <div class="item_fields">
        
        <h3><?php echo $this->item->namerezept; ?></h3>
		<div>
		<?php if($video != null || $image != null):?>
		<?php  if ($video != null):?>
			<div id='my-video'></div>
				<script type='text/javascript'>
					 jwplayer('my-video').setup({
   						 'flashplayer' : 'player.swf',
       					 'file': "<?php echo JURI::root(true).'/components/com_thm_reverscookings/img/rezept_video/'. $video[0]->filename; ?>",
					 'image':"<?php echo JURI::root(true).'/components/com_thm_reverscookings/img/rezept_bild/'.$image[0]->filename; ?>",
        				 'width':'480',
       					 'height':'270'
   						 });
					</script>
		<?php else :?>
		<?php echo JHTML::image('components/com_thm_reverscookings/img/rezept_bild/'.$image[0]->filename, $this->item->namerezept."_Bild", $this->attribs); ?>
		<?php endif;?>
		<?php endif;?>
		</div>
		
		<br/>
		
		<div><h4><?php echo JText::_('ingredientname'); ?></h4>
			<?php echo  Thm_reverscookingsModelreverscookingsrezept::getIngredientformal($this->item->id); ?></div>
			
		<br/>
		
		<div><h4><?php echo JText::_('ZUBEREITUNG'); ?></h4>
			<?php echo $this->item->zubereitung; ?>
		</div>
		
		<table width=600>
			<tr>
				<td><?php echo  JText::_('portionanzahl'); ?> </td>
				<td><?php echo $this->item->portionanzahl; ?></td>
			</tr>
			<tr>	
				<td><?php echo JText::_('ZUBEREITUNGDAUERT'); ?></td>
				<td><?php echo $this->item->zubereitungdauert; ?></td>
			</tr>
			<tr>
				<td><?php echo JText::_('CHECKED_OUT_TIME'); ?></td>
				<td><?php echo $this->item->checked_out_time; ?></td>
			</tr>
			<tr>
				<td><?php echo JText::_('CREATED_BY'); ?></td>
				<td><?php echo Thm_reverscookingsHelper::myprofil($this->item->created_by,'created_by'); ?></td>
			</tr>
			<tr>
				<td><?php echo JText::_('VIEWS'); ?></td>
				<td><?php echo Thm_reverscookingsHelper::viewrezept($this->item->id) ?></td>
			</tr>
			<tr>
				<td><?php echo JText::_('REZEPTKATEGORY'); ?></td>
				<td><?php echo $this->item->rezeptkategory; ?></td>
			</tr>
		</table>
        
  	</div>
  	
  	<br/>
  	
  	<table width=300>
		<tr>
			<td><?php echo JText::_('COM_CODE134'); ?></td>
			<td><?php echo Thm_reverscookingsModelReverscookingsrezept::getRating($this->item->id);?></td>
		</tr>
		<tr>			
			<td><?php echo JText::_('COM_CODE127'); ?></td>
			<td><?php echo Thm_reverscookingsModelReverscookingsrezept::getRatingcount($this->item->id);?></td>
  		</tr>
  		<tr>
  			<td><?php if (!$user->guest) {echo JText::_('COM_CODE128');}?></td>
  			<td><?php 	echo Thm_reverscookingsModelReverscookingsrezept::getUserRating($this->item->id, $user->id);?></td>
  		</tr>
  	</table>	
  	
  	<br/><br/>
  	
    <?php  if(!$user->guest): ?>
	<form name= "rating_form" method= "post" action= "<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.rating'); ?>" >
	    <table width=300>
	    	
			<tr>
				<td valign="bottom">
				    <select name="rating">
							<option value="1">1 Stern</option>
				  			<option value="2">2 Sterne</option>
				 			<option value="3">3 Sterne</option>
				 			<option value="4">4 Sterne</option>
				 			<option value="5">5 Sterne</option>
							<option value="6">6 Sterne</option>
				  			<option value="7">7 Sterne</option>
				 			<option value="8">8 Sterne</option>
				 			<option value="9">9 Sterne</option>
				 			<option value="10">10 Sterne</option>
				 	</select>
				</td>
				<td valign="top">
					<input type="hidden" name="user_id" value="<?php echo JFactory::getUser()->id;?>" >
					<input type="hidden" name="rezept_id" value="<?php echo $this->item->id;?>" >
					<input id= "rating-post" type= "submit" value= "<?php echo JText::_('COM_CODE140');?>">
				</td></tr>
  	</table>
  </form>
	<br/>
		
	<h3><?php echo JText::_('COM_CODE141');?></h3>

	<div>
		<?php echo Thm_reverscookingsModelReverscookingsrezept::getComments($this->item->id,$this->item->created_by);?>
	</div>
	
	<br/>
	

	<form name= "comment_form" method= "post" action= "<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.comment'); ?>" >
	<h3><?php echo JText::_('COM_CODE130');?></h3>
	<input type="hidden" name="user_id" value="<?php echo JFactory::getUser()->id;?>" >
	<input type="hidden" name="rezept_id" value="<?php echo $this->item->id;?>" >
	<div class= "comments-open-content">
		<textarea id= "comment-text" cols= "40" rows= "10" name= "text"></textarea>
	</div>
	<br>
		<input id= "comment-post" type= "submit" value= "Absenden">
	
</form>
<?php  endif; ?>
   
<?php else: ?>
   Sorry the Item not found!!
<?php endif; ?>
