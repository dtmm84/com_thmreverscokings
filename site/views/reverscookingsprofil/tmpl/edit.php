<?php
/**
 * @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_thm_reverscookings', JPATH_ADMINISTRATOR);

$image = Thm_reverscookingsHelper::hasbild('#__thm_reverscookingsprofils_bild', 'profilid', $this->item->id);

?>

<style>
    .front-end-edit ul {
        padding: ;
    }
    .front-end-edit li {
        list-style: none;
        margin-bottom: 6px ;
    }
    .front-end-edit label {
        margin-right: 10px;
        display: block;
        float: left;
        width: 200px ;
    }
    .front-end-edit .radio label {
        display: inline;
        float: none;
    }
    .front-end-edit .readonly {
        border: none ;
        color: #666;
    }    
    .front-end-edit #editor-xtd-buttons {
        height: 50px;
        width: 600px;
        float: left;
    }
    .front-end-edit .toggle-editor {
        height: 50px;
        width: 120px;
        float: right;
        
    }
</style>

<div class="reverscookingsprofil-edit front-end-edit">
    <h1>Edit</h1>

    <form id="form-reverscookingsprofil" action="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsprofil.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <ul>
				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('virtualfridge'); ?>
				<?php echo $this->form->getInput('virtualfridge'); ?></li>
				
				<li>
				<?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('uname'); ?>
				<?php echo $this->form->getInput('uname'); ?>
				</li>
				<li><?php echo $this->form->getLabel('address'); ?>
				<?php echo $this->form->getInput('address'); ?></li>
				<li><?php echo $this->form->getLabel('plz'); ?>
				<?php echo $this->form->getInput('plz'); ?></li>
				<li><?php echo $this->form->getLabel('ort'); ?>
				<?php echo $this->form->getInput('ort'); ?></li>
				<li><?php echo $this->form->getLabel('email'); ?>
				<?php echo $this->form->getInput('email'); ?>
				</li>
				<li><?php echo JText::_('BILD'); ?>:<?php echo JHTML :: image('components/com_thm_reverscookings/img/profil_bild/'.$image[0]->filename, $this->item->uname."_Bild", $this->attribs); ?><br/>
				<input type="file" name = "image_upload" /></li>
				
				<li><?php echo $this->form->getLabel('description'); ?>
				<?php echo $this->form->getInput('description'); ?></li>
				<li><?php echo $this->form->getLabel('state'); ?>
				<?php echo $this->form->getInput('state'); ?></li>
				

        </ul>
		<div>
			<button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
			<?php echo JText::_('or'); ?>
		<a type="button" href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsprofil.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
		<?php echo JText::_('or'); ?>
		<a type ="button"  href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsprofil.delete&id='.$this->item->id); ?>" title="<?php echo JText::_('Delete'); ?>"><?php echo JText::_('Delete'); ?></a>
			
			<input type="hidden" name="option" value="com_thm_reverscookings" />
			<input type="hidden" name="task" value="reverscookingsprofil.save" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
