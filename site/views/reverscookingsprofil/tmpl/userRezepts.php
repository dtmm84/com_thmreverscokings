<?php
/**
 * @version     1.1.0
* @package     com_thm_reverscookings
* @copyright   Copyright (C) 2012. All rights reserved.
* @license     GNU General Public License
*  @author Bassing <dominik.bassing@mni.thm.de>
*  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
*  @author Schneider <stefan.schneider@mni.thm.de>
*  @author Omoko <guy.bertrand.omoko@hotmail.com>
*  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
*  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
*/

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_thm_reverscookings', JPATH_ADMINISTRATOR);

$app= JRequest::get( 'get' );
$sort = $app['sort'];

 
?>
<table class="adminlist">
		<thead>
			<tr>
				<th style="background: #ccc;"><a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&sort=namerezept');?>"><?php echo JText::_("COM_REVERSCOOKING_RECIPENAME");  ?></a>
				</th>
				<th style="background: #ccc;"><a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&sort=zubereitungdauert');?>"><?php echo JText::_("COM_REVERSCOOKING_RECIPE_PREPERATIONTIME");  ?></a>
				</th>
				<th style="background: #ccc;"><a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&sort=views');?>"><?php echo JText::_("COM_REVERSCOOKING_RECIPE_VIEWS");  ?></a>
				</th>
				<th style="background: #ccc;"><a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&sort=checked_out_time');?>"><?php echo JText::_("COM_REVERSCOOKING_RECIPE_PREPERATIONTIME");  ?></a>
				</th>
				<th style="background: #ccc;"><a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&sort=state');?>"><?php echo JText::_("STATE");  ?></a>
				</th>
				<th style="background: #ccc;"><a><?php echo JText::_("COM_CODE120");  ?></a>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php echo Thm_reverscookingsHelper::userRezepts($this->item->created_by, $sort);?>
		</tbody>
		</table>