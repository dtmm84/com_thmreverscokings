<?php
/**
 * @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_thm_reverscookings', JPATH_ADMINISTRATOR);
$session =& JFactory::getSession();
$session->set('reverscookingsprofils', array());
$image = Thm_reverscookingsHelper::hasbild('#__thm_reverscookingsprofils_bild', 'profilid', $this->item->id);

?>
<script type="text/javascript">


function deleteing (id){
	
	var temp = 'delete'+id;
	
	var profilid = document.getElementById(temp).value;


	var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsprofil.deleteing';

	 var data = 'ingid='+id+'&profilid='+profilid;
	 document.getElementById('content-ing').innerHTML="";
     var request = new Request({

     url: url,

     method:'get',

     data: data,

     onSuccess: function(responseText){

document.getElementById('content-ing').innerHTML=  responseText;

     }

     }).send();
}

function runButton() {
    
    var ingame = document.getElementById("ingname").value;

    document.getElementById("ingname").value="";

    var ingquantity = document.getElementById("ingquantity").value;
    
    document.getElementById("ingquantity").value="";
    

    var ingunit = document.getElementById("ingunit").value;

    document.getElementById("ingunit").value="";

    var ingid = document.getElementById("einfuegen").value;

    if(ingid == "einfuegen"){
        alert("<?php echo JText::_('COM_CODE122');?>");
    	ingid =0;
    	 return;
    }
   
    if(ingame=="" || ingquantity=="" || ingunit==""){
        alert("<?php echo JText::_('COM_CODE123');?>");
        return;
    }
		 tempquantity = parseInt(ingquantity);
		
	if(isNaN(tempquantity)){
        alert("<?php echo JText::_('COM_CODE124');?>");
        return;
    	}
    
    
    var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsprofil.listFields';

    var data = 'ingid='+ingid+'&ingname='+ingame+'&ingquantity='+ingquantity+'&ingunit='+ingunit;

                        var request = new Request({

                        url: url,

                        method:'get',

                        data: data,

                        onSuccess: function(responseText){

						document.getElementById('fields-container').innerHTML=  responseText;

                        }

                        }).send();

}

	function deleteKorb( id){
		var korbindex=id;
		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsprofil.deleteKorb';

		var data = 'korbindex='+korbindex;

		  var request = new Request({

              url: url,

              method:'get',

              data: data,

              onSuccess: function(responseText){

document.getElementById('fields-container').innerHTML=  responseText;

              }

              }).send();

		

	}
	
function autocom(){
		
		var teil = document.getElementById("ingname").value;

		if(teil.length >0){

		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsprofil.autocomplete';

		 var data = 'ingname='+teil;
		 
		  var request = new Request({

              url: url,

              method:'get',

              data: data,

              onSuccess: function(responseText){

document.getElementById('ing-container').innerHTML=  responseText;

              }

              }).send();
		}
		
	}
	
	function ausgewaehlt(ingid){
	   var ingredient = document.getElementById(ingid).value;
		var arr = ingredient.split(":");
		document.getElementById("ingname").value = arr[0];
		document.getElementById("ingunit").value = arr[1];
		document.getElementById("einfuegen").value = ingid;
		document.getElementById('ing-container').innerHTML='';
	}

	function kochen(rezeptid){
		var profilid= document.getElementById("kochen").value;
		var portionzahl = document.getElementById("portionzahl").value;
		
		if(isNaN(portionzahl)){
	        alert("<?php echo JText::_('COM_CODE125');?>");
	        return;
	    	}
    	
		if(portionzahl == 0){
			alert("<?php echo JText::_('COM_CODE126');?>");
			return;
		}
		
		var url='index.php?option=com_thm_reverscookings&format=raw&task=reverscookingsprofil.kochen';

		var data = 'profilid='+profilid+'&rezeptid='+rezeptid+'&portion='+portionzahl;
		document.getElementById('content-ing').innerHTML="";
		  var request = new Request({url: url,
			  						 method:'get',
			  						 data: data,
			  						 onSuccess: function(responseText){

			  							document.getElementById('content-ing').innerHTML=  responseText;

			  							     }
			  						 }).send();
		
		
	}
	
	 
</script>
<div>
	    <?php if(Thm_reverscookingsHelper::isMyprofil(JFactory::getUser()->id, $this->item->id)): ?>
			<a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsprofil.edit&id='.$this->item->id); ?>"><H2><u><?php echo JText::_('COM_CODE138');?></u></H2></a>
		<?php endif; ?>
</div>
<br/>
<H3><font color="#0088CC"><?php echo JText::_('COM_CODE139');?></font></H3>
<br/>
<div>
<?php if(!JFactory::getUser()->guest):?>
<?php if( isset($this->item->id) ) : ?>
        
       <!--  <ul class="fields_list">
			 <li><?php echo JText::_('ORDERING'); ?>:
			<?php echo $this->item->ordering; ?></li>
			<li><?php echo JText::_('STATE'); ?>:
			<?php echo $this->item->state; ?></li>
			<li><?php echo JText::_('CHECKED_OUT'); ?>:
			<?php echo $this->item->checked_out; ?></li>
			<li><?php echo JText::_('CHECKED_OUT_TIME'); ?>:
			<?php echo $this->item->checked_out_time; ?></li>
			<li><?php echo JText::_('CREATED_BY'); ?>:
			<?php echo $this->item->created_by; ?></li>
			<li><?php echo JText::_('UNAME'); ?>:
			<?php echo $this->item->uname; ?></li>
			<li><?php echo JText::_('ADDRESS'); ?>:
			<?php echo $this->item->address; ?></li>
			<li><?php echo JText::_('PLZ'); ?>:
			<?php echo $this->item->plz; ?></li>
			<li><?php echo JText::_('ORT'); ?>:
			<?php echo $this->item->ort; ?></li>
			<li><?php echo JText::_('EMAIL'); ?>:
			<?php echo $this->item->email; ?></li>
			<li><?php echo JText::_('BILD'); ?>:
			<?php echo JHTML :: image('components/com_thm_reverscookings/img/profil_bild/'.$image[0]->filename, $this->item->uname."_Bild", $this->attribs); ?></li>
			<li><?php echo JText::_('VIRTUALFRIDGE'); ?>:
			<?php echo $this->item->virtualfridge; ?></li>
			<li><?php echo JText::_('DESCRIPTION'); ?>:
			<?php echo $this->item->description; ?></li>
        </ul> -->
        
        
    	<?php if ($this->item->virtualfridge == 1):?>
			<div>
				 <?php echo Thm_reverscookingsModelReverscookingsprofil::getUserIngredient($this->item->id);?>
			</div>			
		<?php endif; ?>
		<?php else: ?>
		   <a href="<?php echo JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsprofil.edit&id='.$this->item->id); ?>"><H3><?php echo JText::_('COM_CODE144');?></H3></a>
		<?php endif; ?>
		<?php else: ?>
		    This is only for registrie User!!
		<?php endif; ?>
	    

</div>
