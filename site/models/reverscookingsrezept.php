<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>  
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';

/**
 * Thm_reverscookings model.
 */
class Thm_reverscookingsModelReverscookingsrezept extends JModelForm
{
    
    var $_item = null;
    
	/**
	 * Method to auto-populate the model state.
	 *
	 *
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_thm_reverscookings');

		// Load state from the request userState on edit or from the passed variable on default
        if (JFactory::getApplication()->input->get('layout') == 'edit'  ) {
            $id = JFactory::getApplication()->getUserState('com_thm_reverscookings.edit.reverscookingsrezept.id');
        } else {
            $id = JFactory::getApplication()->input->get('id');
            JFactory::getApplication()->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.id', $id);
            
        }
        if (JFactory::getApplication()->input->get('layout') == 'new'  ){
        	$id='0';
        	$app->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.id', $id);
        }
		$this->setState('reverscookingsrezept.id', $id);
		
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

	}
        

	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id)) {
				$id = $this->getState('reverscookingsrezept.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published) {
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			} elseif ($error = $table->getError()) {
				$this->setError($error);
			}
		}

		return $this->_item;
	}
    
	public function getTable($type = 'Reverscookingsrezept', $prefix = 'Thm_reverscookingsTable', $config = array())
	{   
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        return JTable::getInstance($type, $prefix, $config);
	}     

    
	/**
	 * Method to check in an item.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int)$this->getState('reverscookingsrezept.id');

		if ($id) {
            
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
            if (method_exists($table, 'checkin')) {
                if (!$table->checkin($id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int)$this->getState('reverscookingsrezept.id');

		if ($id) {
            
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
            if (method_exists($table, 'checkout')) {
                if (!$table->checkout($user->get('id'), $id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
		}

		return true;
	}    
    
	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML 
     * 
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_thm_reverscookings.reverscookingsrezept', 'reverscookingsrezept', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 */
	protected function loadFormData()
	{
		$data = $this->getData(); 
        
        return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 */
	public function save($data)
	{
		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('reverscookingsrezept.id');
        $user = JFactory::getUser();

        if($id) {
            //Check the user can edit this item
            $authorised = Thm_reverscookingsHelper::isMyrezept($user->id, $id);
        } else {
            //Check the user can create new items in this section
            $authorised = !$user->guest;
        }

        if ($authorised !== true) {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }

		$table = $this->getTable();
        if ($table->save($data) === true) {
            return $id;
        } else {
            return false;
        }
        
	}
public function getIngredient($itemid ){
		$result = '<div id ="content-ing" ><table width="96%"><tr><th width="30%">'.JText::_('COM_CODE117').'</th><th width="30%">'.JText::_('COM_CODE118').'</th><th width="30%">'.JText::_('COM_CODE119').'</th><th>Option</th></tr>';
		$db = JFactory::getDBO();
		$query = 'SELECT rezeptingredient.id AS id, ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, rezeptingredient.quantity AS ingquantity
		FROM  #__thm_reverscookings_ingredients_rezept AS rezeptingredient LEFT JOIN  #__thm_reverscookings_rezept AS rezepts
		ON rezeptingredient.rezeptid = rezepts.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON rezeptingredient.ingid = ingredients.id WHERE rezepts.id = '.$itemid;
		$db->setQuery($query);
		$ingredients = $db->loadObjectList();
		if(!empty($ingredients)){
			foreach ($ingredients as $ing){
				$del= 'delete'.$ing->id;
				$result .='<tr><td width="20%">'.$ing->ingname.'</td><td width="20%"><input type="text" id="quantity"  name= ingredient['.$ing->id.']'.' value='.$ing->ingquantity.' /></td>
				<td width="20%">'.$ing->ingunit.'</td><td><button  id='.$del.'  value='.$itemid.' onclick="deleteing('.$ing->id.')">'.JText::_('COM_CODE121').'</button></td></tr>';
			}
		}
		$result .='</table></div>';
	
		$result .= '<div id = "fields-container"></div>';
	
		$result.='<table border="0" float ="center"><tr><td>
		<input type="text" onkeyup="autocom()"  value="" id="ingname" maxlength="255"></td>
		<td><input type="text" value="" id="ingquantity" maxlength="15"></td>
		<td><input type="text" value="" id="ingunit"></td><td border="0"><button type="button" id="einfuegen" value="einfuegen" onClick="runButton()">'.JText::_('COM_CODE115').'</button></td></tr></table>';
		$result.='<div id="ing-container" style="display:block align:center"></div>';
	
	
		return $result;
	}
	public function getIngredientformal($itemid){
		$result = '<div><table width= "50%" ><tr><th width="20%">'.JText::_('COM_CODE117').'</th><th width="20%">'.JText::_('COM_CODE118').'</th><th width="20%">'.JText::_('COM_CODE119').'</th></tr>';
		$db = JFactory::getDBO();
		$query = 'SELECT rezeptingredient.id AS id, ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, rezeptingredient.quantity AS ingquantity
		FROM  #__thm_reverscookings_ingredients_rezept AS rezeptingredient LEFT JOIN  #__thm_reverscookings_rezept AS rezepts
		ON rezeptingredient.rezeptid = rezepts.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON rezeptingredient.ingid = ingredients.id WHERE rezepts.id = '.$itemid;
		$db->setQuery($query);
		$ingredients = $db->loadObjectList();
		if(!empty($ingredients)){
			foreach ($ingredients as $ing){
				$result .='<tr><td width= "20%">'.$ing->ingname.'</td> <td width= "20%">'.$ing->ingquantity.'</td> <td width= "20%">'.$ing->ingunit.'</td> </tr>';
			}
		}
		$result .='</table></div>';
	
	
		return $result;
	}
	
	public static function getComments($id,$createdby){
		$akuser = JFactory::getUser()->id;
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
	
		$query->select('a.*')->from('#__thm_reverscookings_comment_rezept as a');
		$query->select('u.name AS autor');
		$query->join('LEFT', '#__users AS u ON u.id = a.userid');
		$query->select('r.namerezept AS rezept_name');
		$query->join('LEFT', '#__thm_reverscookings_rezept AS r ON a.rezeptid = r.id');
		$query->where('a.rezeptid = '.(int) $id);
		$db->setQuery($query);
		$comments = $db->loadObjectList();
		if(!empty($comments)){
			foreach ($comments as $comment){
				if(($db->escape($comment->comment, true)) == NULL)
					continue;
				$result .= '</br><div><p>'. $comment->comment.'</p></br>'.JText::_('COM_CODE102'). $comment->autor. '</br>'.JText::_('COM_CODE103').': '.$comment->date_create;
				
				if($comment->userid == $akuser || $akuser == $createdby){
					$result .='<form name= "deletecomment" method="post" action= "'. JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsrezept.deletecomment').'" >';
					$result .= '<input type="hidden" name="user_id" value="'.$akuser.'" >';
					$result .= '<input type="hidden" name="commentid" value="'.$comment->id.'" >';
					$result .= '<input type="hidden" name="rezeptid" value="'.$id.'" >';
					$result.='<input id= "rating-post" type= "submit" value= "'. JText::_('COM_CODE121').'"></form>';
				}
				
				$result.='</br>';
			}
			$result .= '</div>';
		}
		return $result;
	}
	
	public static function getRating($id){
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = 'SELECT AVG(bewertung) AS bewertung
				FROM  #__thm_reverscookings_bewertung_rezept
				WHERE rezeptid = '.$id;
		$db->setQuery($query);
		$ratings = $db->loadObjectList();
		if(!empty($ratings)){
			foreach ($ratings as $rating){
				$result = $rating->bewertung;
			}
		}
		
		$result .= '</div>';
		return round($result,2);
	}

	public static function getRatingcount($id){
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = 'SELECT COUNT(bewertung) AS bewertung
				FROM  #__thm_reverscookings_bewertung_rezept
				WHERE rezeptid = '.$id;
		$db->setQuery($query);
		$ratings = $db->loadObjectList();
		if(!empty($ratings)){
			foreach ($ratings as $rating){
				$result = $rating->bewertung;
			}
		}
	
		$result .= '</div>';
		return $result;
	}
	
	public static function getUserRating($id,$user){
		$result ='<div>';
		$db = JFactory::getDbo();
		$query = 'SELECT bewertung AS bewertung
				FROM  #__thm_reverscookings_bewertung_rezept
				WHERE rezeptid = '.$id .' AND  userid = '.$user;
		$db->setQuery($query);
		$ratings = $db->loadObjectList();
		if(!empty($ratings)){
			foreach ($ratings as $rating){
				$result = $rating->bewertung;
			}
		}
	
		$result .= '</div>';
		return $result;
	}
	
}