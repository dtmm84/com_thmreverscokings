<?php
/**
 * @version     1.1.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';
/**
 * Thm_reverscookings model.
 */
class Thm_reverscookingsModelReverscookingsprofil extends JModelForm
{
    
    var $_item = null;
    
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_thm_reverscookings');
		$hasprofil = Thm_reverscookingsHelper::hasprofil(JFactory::getUser()->id);
		// Load state from the request userState on edit or from the passed variable on default
        if (JFactory::getApplication()->input->get('layout') == 'edit') {
            $id = JFactory::getApplication()->getUserState('com_thm_reverscookings.edit.reverscookingsprofil.id');
        } else {
            $id = JFactory::getApplication()->input->get('id');
            JFactory::getApplication()->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.id', $id);
        }
        if($id == NULL && $hasprofil != FALSE)
        	$id= $hasprofil[0]->id;
		$this->setState('reverscookingsprofil.id', $id);
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

	}
        

	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id)) {
				$id = $this->getState('reverscookingsprofil.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published) {
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			} elseif ($error = $table->getError()) {
				$this->setError($error);
			}
		}

		return $this->_item;
	}
    
	public function getTable($type = 'Reverscookingsprofil', $prefix = 'Thm_reverscookingsTable', $config = array())
	{   
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        return JTable::getInstance($type, $prefix, $config);
	}     

    
	/**
	 * Method to check in an item.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int)$this->getState('reverscookingsprofil.id');

		if ($id) {
            
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
            if (method_exists($table, 'checkin')) {
                if (!$table->checkin($id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int)$this->getState('reverscookingsprofil.id');

		if ($id) {
            
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
            if (method_exists($table, 'checkout')) {
                if (!$table->checkout($user->get('id'), $id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
		}

		return true;
	}    
    
	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML 
     * 
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_thm_reverscookings.reverscookingsprofil', 'reverscookingsprofil', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 */
	protected function loadFormData()
	{
		$data = $this->getData(); 
        
        return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 */
	public function save($data)
	{
		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('reverscookingsprofil.id');
        $user = JFactory::getUser();
        $userid= $user->id;
        if($id) {
            //Check the user can edit this item
            $ismin = Thm_reverscookingsHelper::isMyprofil($userid, $id);
            $authorised = $ismin;
            
        } else {
            //Check the user can create new items in this section
            if(Thm_reverscookingsHelper::hasprofil($userid)==FALSE)
            $authorised = true;
        }

        if ($authorised !== true) {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }

		$table = $this->getTable();
        if ($table->save($data) === true) {
            return $id;
        } else {
            return false;
        }
        
	}
public static  function getUserIngredient($itemid){
		$result='';
		$ismyprofil = Thm_reverscookingsHelper::isMyprofil(JFactory::getUser()->id, $itemid);
		$result .= '<form id="form-reverscookingsrezept" action= "'. JRoute::_('index.php?option=com_thm_reverscookings&task=reverscookingsprofil.saveing').'" method="post" class="item_fields">
		<input type="hidden" name="profilid" value="'.$itemid.'" >';
		
		$result .= '<div id ="content-ing" ><table width="96%"><tr><th width="30%">'.JText::_('COM_CODE117').'</th><th width="30%">'.JText::_('COM_CODE118').'</th><th width="30%">'.JText::_('COM_CODE119').'</th>';
		if($ismyprofil)
		$result .='<th>'.JText::_('COM_CODE120').'</th></tr>';
		else{$result.= '</tr>';}
		$itemid = intval($itemid);
		$ingredients = self::getprofilning($itemid);
		$tempiding=array();
		
		if(!empty($ingredients)){
			$whererequest = ' a.ingid=';
			for ($i=0; $i<count($ingredients); $i++){
				$ing=$ingredients[$i];
				$temp= new stdClass();
				$temp->id = $ing->ingid;
				$temp->value = $ing->ingquantity;
				array_push($tempiding, $temp);
				$whererequest .=$ing->ingid;
				if($i< count($ingredients)-1)
					$whererequest.=' OR  a.ingid=';
				$del= 'delete'.$ing->id;
				$result .='<tr><td width="20%">'.$ing->ingname.'</td><td width="20%"><input type="text" id="quantity"  name= ingredient['.$ing->id.']'.' value='.$ing->ingquantity.' /></td>
				<td width="20%">'.$ing->ingunit.'</td>';
				if($ismyprofil)
				$result .='<td><button  id='.$del.'  value='.$itemid.' onclick="deleteing('.$ing->id.')">'.JText::_('COM_CODE121').'</button></td></tr>';
				else {
					$result .='</tr>';
				}
			}
		}
		
		$result .='</table></div>';
		
		
		
		$result .= '<div id = "fields-container"></div>';
		if($ismyprofil){
		$result.='<table  style="padding:20; margin:10;"><tr></tr><tr><td  width="20%">
		<input type="text" onkeyup="autocom()" value="" id="ingname" maxlength="255"></td>
		<td width="20%" ><input type="text" value="" id="ingquantity" maxlength="15"></td>
		<td width="20%" ><input type="text" value="" id="ingunit" maxlength="15"></td>
		<td><button type="button" id="einfuegen" value="einfuegen" onClick="runButton()">'.JText::_('COM_CODE115').'</button></td></tr></table>';
		$result.='<div id="ing-container"></div>';
		
		}
		
		if($ismyprofil){
			
			$result.='<button type="submit" class="validate"><span>'.JText::_('COM_CODE116').'</span></button>';
		}
		$result .='</form>';
		 $result.='<br/><br/><br/>';
		 $result.='<h3><font color="#0088CC">'.JText::_('COM_CODE143').'</h3>';
		 $result.=self::rezeptuser($tempiding,$whererequest,$itemid,$ismyprofil);
		 
		return $result;
	}    
	public static function getprofilning($itemid){
		$db = JFactory::getDBO();
		$query = 'SELECT usering.id AS id,usering.ingid as ingid , ingredients.ingname AS ingname, ingredients.ingunit
		AS ingunit, usering.quantity AS ingquantity
		FROM  #__thm_reverscookings_virtual_fridge AS usering LEFT JOIN  #__thm_reverscookingsprofils AS profils
		ON usering.userid = profils.id LEFT JOIN #__thm_reverscookings_ingredients AS ingredients
		ON usering.ingid = ingredients.id WHERE profils.id = '.$itemid;
		$db->setQuery($query);
		$db->query();
		return  $db->loadObjectList();
		
	}
	
	public function rezeptuser($ingarray, $ingrequest,$profilid,$ismyprofil){
		$result ='<div><table width="100%" border="0" ><tr><th></th><th></th>';
		if($ismyprofil){
		$result.='<th></th><th></th></tr>';
		}
		else {$result .= '</tr>';
		}
		if(empty($ingrequest))
			return null;
		$inganzahl = count($ingarray);
		$rezepts = self::getrezept($ingrequest,$inganzahl);
		
		if($rezepts != null){
		foreach ($rezepts as $rezt){
			$rezIng = self::getInganzahl($rezt->rezeptsid);
			$bewertung = Thm_reverscookingsHelper::getRating($rezt->rezeptsid);
			$result .='<tr><td width="50%" cellpadding="10"><a href= '.JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id='. $rezt->rezeptsid).
			'><b>'.$rezt->rezeptsname.'</b></a></td><td width="70%" cellpadding="10"> '.JText::_('COM_CODE110').' <b style="color:red;">'.$rezt->treffer.'</b>/'.$rezIng.JText::_('COM_CODE111').$rezt->views.JText::_('COM_CODE112').$bewertung.
			'</td>';
			if($ismyprofil){
				$result.='<td><input type="text" id="portionzahl" value=""></td><td><button id=kochen value="'.$profilid.'" onclick="kochen('.$rezt->rezeptsid.')">'.JText::_('COM_CODE113').'</button></td></tr>';
			}
			else {$result .='</tr>';}
		}
		}
		$result.='</table></div>';
	
		return $result;
	}
	
	public function getrezept( $ingrequest,$inganzahl){
		$db = JFactory::getDbo();
		$query = 'SELECT  rezept.id as rezeptsid, rezept.namerezept as rezeptsname, count(a.ingid) as treffer, rezept.views as views 
				FROM #__thm_reverscookings_ingredients_rezept as a left join #__thm_reverscookings_rezept as rezept on a.rezeptid = rezept.id   
				WHERE '.$ingrequest.' group by rezeptsid order by  treffer desc';
		
		$a=$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	public function getInganzahl($rezid){
		$db = JFactory::getDbo();
		$query= 'SELECT count(ingid) as zahl FROM  #__thm_reverscookings_ingredients_rezept WHERE  rezeptid='.$rezid;
		 $db->setQuery($query);
		$a = $db->loadObjectList();
		return $a[0]->zahl;
		
	}
    
}