<?php
/**
 * @version     1.0.0
* @package     com_thm_reverscookings
* @copyright   Copyright (C) 2012. All rights reserved.
* @license     GNU General Public License
*  @author Bassing <dominik.bassing@mni.thm.de>
*  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
*  @author Schneider <stefan.schneider@mni.thm.de>
*  @author Omoko <guy.bertrand.omoko@hotmail.com>
*  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
*  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

JHTML::_('behavior.mootools');

require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';


JHTML::_('behavior.framework', true);

/**
 * Reverscookingsrezept controller class.
 */
class Thm_reverscookingsControllerReverscookingsrezept extends JControllerForm
{

	function __construct() {
		$this->view_list = 'reverscookingsrezepts';
		parent::__construct();
	}

	public function listFields() {

		$ingname = JRequest::getVar( 'ingname', '', 'get', 'cmd' ) ;

		$ingquantity = JRequest::getVar( 'ingquantity', '', 'get', 'cmd' ) ;
		
		$ingunit = JRequest::getVar( 'ingunit', '', 'get', 'cmd' ) ;
		
		$ingid = JRequest::getVar( 'ingid', '', 'get', 'cmd' ) ;
	
		$data = array($ingname,$ingquantity,$ingunit);
		$session =& JFactory::getSession();
		$rezepting = $session->get('reverscookingsrezept');
		$element= new stdClass();
		$element->id=$ingid;
		$element->value =$data;
		array_push($rezepting, $element);
		$session->set('reverscookingsrezept', $rezepting);
		$result = '<li><label>Neue Ingredients</label></li>';
		$result = '<li><table width= "50%" >';
		foreach ($rezepting as $element ){
			$index=$element->id;
			$ing=$element->value;
			if(!empty($ing[1])){
			$result.='<tr><td width= "20%">'.$ing[0].'</td>
			<td width= "20%">'.$ing[1].'</td>
			<td width= "20%">'.$ing[2].'</td>
			<td width= "10%" ><button  type=button Label = '.$ing[0].' name ='.$ing[0].' onclick= "deleteKorb('.$index.')" value = '.$ing[0].'>delete</button></td></tr>';
			}
		}
		$result .= '</table></li>';
		echo $result ;


	}
	
	public function deleteKorb (){
		$newrezepting = array();
		$ingindex = JRequest::getVar( 'korbindex', '', 'get', 'cmd' ) ;
		$session =& JFactory::getSession();
		$rezepting = $session->get('reverscookingsrezept');
		if(count($rezepting)>1){
		unset($rezepting[$ingindex]);
		$newrezepting = array_values($rezepting);
		}
		
		$session->set('reverscookingsrezept', $newrezepting);
		$result = '<li><label>Neue Ingredients</label></li>';
		$result = '<li><table width= "50%" >';
		foreach ($newrezepting as $i=>$element){
			$ing = $element->value;
			if(!is_numeric($ing[0])){
			$result.='<tr><td width= "20%">'.$ing[0].'</td>
			<td width= "20%">'.$ing[1].'</td>
			<td width= "20%">'.$ing[2].'</td>
			<td width= "10%"><button  type=button Label = '.$ing[0].' name ='.$ing[0].' onclick= "deleteKorb('.$i.')" value = '.$ing[0].'>delete</button></td></tr>';
			}
		}
		$result .= '</table></li>';
		echo $result ;

	}
	
	public  function deleteing(){
		$ingindex = intval(JRequest::getVar( 'ingid', '', 'get', 'cmd' )) ;
		$rezeptid = intval(JRequest::getVar('rezeptid','','get','cmd'));
		$temp = Thm_reverscookingsHelper::deleteingrezept($ingindex);
		$result = Thm_reverscookingsHelper::newinglist($rezeptid);
		echo $result ;
	}
	
	public function autocomplete(){
		$teilingname = JRequest::getVar( 'ingname', '', 'get', 'cmd' ) ;
		$ingredients = Thm_reverscookingsHelper::inglikename($teilingname);
		$result='<li><select size="10">';
		
	foreach ($ingredients as $item){
			$value = $item->ingname.':'.$item->ingunit;
			$result.='<option  id='.$item->id.' value='.$value.'
			style="cursor:pointer" onclick="ausgewaehlt('.$item->id.')" >'.$item->ingname.':'.$item->ingunit.'</option>';
		}
		$result.='</select></li>';
		echo $result ;
	}

}