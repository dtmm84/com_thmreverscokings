<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// No direct access
defined('_JEXEC') or die;

ini_set("upload_max_filesize",3000);

ini_set("post_max_size",800);


require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';


/**
 * Reverscookingsrezept controller class.
 */
class Thm_reverscookingsControllerReverscookingsrezept extends Thm_reverscookingsController
{

	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 */
	public function edit()
	{
		$app= JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_thm_reverscookings.edit.reverscookingsrezept.id');
		$editId	= JFactory::getApplication()->input->getInt('id', null, 'array');

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.id', $editId);

		// Get the model.
		$model = $this->getModel('Reverscookingsrezept', 'Thm_reverscookingsModel');

		// Check out the item
		if ($editId) {
            $model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId) {
            $model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return	void
	 */
	public function save()
	{
		self::saveingredient();
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app	= JFactory::getApplication();
		$model = $this->getModel('Reverscookingsrezept', 'Thm_reverscookingsModel');

		// Get the user data.
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');
		$datetime = $data['checked_out_time'];
		$rid= intval($data['id']);
		$user_id = $data['created_by'];
		
		

		// Validate the posted data.
		$form = $model->getForm();
		if (!$form) {
			JError::raiseError(500, $model->getError());
			return false;
		}

		// Validate the posted data.
		$data = $model->validate($form, $data);

		// Check for errors.
		if ($data === false) {
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.data', $data);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_thm_reverscookings.edit.reverscookingsrezept.id');
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&layout=edit&id='.$id, false));
			return false;
		}

		// Attempt to save the data.
		$return	= $model->save($data);

		// Check for errors.
		if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.data', $data);

			// Redirect back to the edit screen.
			$id = (int)$app->getUserState('com_thm_reverscookings.edit.reverscookingsrezept.id');
			$this->setMessage(JText::sprintf('Save failed', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&layout=edit&id='.$id, false));
			return false;
		}
		
		
		
        // Check in the profile.
        if ($return) {
            $model->checkin($return);
        }
        if($rid == 0){
        	$maxid =Thm_reverscookingsHelper::maxid('#__thm_reverscookings_rezept');
        	$rid = $maxid[0]->id ;
        }
        Thm_reverscookingsHelper::updatetime($rid, $datetime, '#__thm_reverscookings_rezept') ;
        Thm_reverscookingsHelper::savebild($user_id, $rid,true);
        Thm_reverscookingsHelper::savevideo($user_id, $rid);
        // Clear the profile id from the session.
        $app->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.id', null);

        // Redirect to the list screen.
        $this->setMessage(JText::_('Item saved successfully'));
        $this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezepts', false));

		// Flush the data from the session.
		$app->setUserState('com_thm_reverscookings.edit.reverscookingsrezept.data', null);
	}
    
    
    function cancel() {
        
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezepts', false));
        
    }
    function delete() {
    	$app= JRequest::get( 'get' );
    	$id = $app['id'];
    	$user = JFactory::getUser();
    	$result = Thm_reverscookingsHelper::isMyrezept($user->id, $id);
    
    	if($result){
    		$db = JFactory::getDBO();
    		Thm_reverscookingsHelper::deletedata($id, "rezept_id", "#__thm_reverscookings_rezept_video");
    		Thm_reverscookingsHelper::deletedata($id, "rezept_id", "#__thm_reverscookingsprofils_bild");
    		$query = "DELETE FROM #__thm_reverscookings_bewertung_rezept WHERE rezeptid=".$id;
    		$db->setQuery($query);
    		$db->query();
    		$query = "DELETE FROM #__thm_reverscookings_comment_rezept WHERE rezeptid=".$id;
    		$db->setQuery($query);
    		$db->query();
    		$query = "DELETE FROM #__thm_reverscookings_ingredients_rezept WHERE rezeptid=".$id;
    		$db->setQuery($query);
    		$db->query();
    			
    		$query = "DELETE FROM #__thm_reverscookings_rezept WHERE id=".$id;
    		$db->setQuery($query);
    		$db->query();
    		$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezepts', false));
    	}
    
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezepts', false));
    }
    
    function saveingredient (){
    	$post = JRequest::get( 'post' );
    	if(empty($post))
    		return;
    	 
    	if($post['task']=='save'||$post['task']=='apply'){
    		$db = JFactory::getDBO();
    		$session =& JFactory::getSession();
    		$ingkorb = $session->get('reverscookingsrezept');
    		$rezeptingredient = $post['ingredient'];
    		$rezeptid = $post['jform']['id'];	
    		
    		if(!empty($rezeptingredient)){
    			foreach ($rezeptingredient as $index => $value){
    				$quantity = intval($value);
    				$query = 'UPDATE #__thm_reverscookings_ingredients_rezept  SET quantity ='.$quantity.' WHERE id ='.$index;
    				$db->setQuery($query);
    				$db->query();
    				$db->loadResult();
    			}
    		}
    		if(!empty($ingkorb)){
    			if($rezeptid ==0){
    				$id = Thm_reverscookingsHelper::maxid('#__thm_reverscookings_rezept');
    				$rezeptid = $id[0]->id + 1;
    				 
    			}
    			foreach ($ingkorb as $element ){
    				$indexs= $element->id ;
    				$ingvalue= $element->value;
    				if($indexs !=0 && !empty($ingvalue[1])){
    					$query = "INSERT #__thm_reverscookings_ingredients_rezept(ingid,rezeptid,quantity)
    					VALUES ('$indexs' , '$rezeptid' , '$ingvalue[1]')";
    					$db->setQuery($query);
    					$db->query();
    				}
    
    			}
    		}
    	}
    	
    }
    
    function comment(){
    	$post = JRequest::get('post');
    	if(empty($post))
    		return;
    	$receptId= $post['rezept_id'];
    	$userId= $post['user_id'];
    	$textInhalt= $post['text'];
    
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    
    	$query->insert("#__thm_reverscookings_comment_rezept (rezeptid, userid, comment) values ('.$receptId.', '.$userId.', '$textInhalt')");
    	$db->setQuery($query);
    	$db->query();
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id='.$receptId, false));
    
    }
    
     function  deletecomment(){
    	$post = JRequest::get('post');
    	if(empty($post))
    		return;
    	$receptId= intval( $post['rezeptid']);
    	$userId= intval($post['user_id']);
    	$commentId= intval($post['commentid']);
    	$db = JFactory::getDbo();
    	$is = Thm_reverscookingsHelper::isMyrezept($userId, $receptId);
    	if($is == true)
    		$query = 'DELETE FROM #__thm_reverscookings_comment_rezept  WHERE id ='.$commentId;
    	else{
    		$query =  'DELETE FROM #__thm_reverscookings_comment_rezept  WHERE id ='.$commentId.' AND userid ='.$userId.' AND rezeptid ='.$receptId;
    	}
    	$db->setQuery($query);
    	$db->query();
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id='.$receptId, false));
    	
    }
    
    function rating(){
    	$post = JRequest::get('post');
    	if(empty($post))
    		return;
    	$receptId= $post['rezept_id'];
    	$userId= $post['user_id'];
    	$rating= $post['rating'];
    
    	$db = JFactory::getDbo();
    	
    	$query = 'SELECT *
				FROM  #__thm_reverscookings_bewertung_rezept
				WHERE userid = '.$userId .' AND  rezeptid = '.$receptId;
    	$db->setQuery($query);
    	$ratings = $db->loadObjectList();
    	if(!empty($ratings)){
    		$query = $db->getQuery(true);
    		$query->update('#__thm_reverscookings_bewertung_rezept AS a')->set('a.bewertung = '.$rating)->where('a.userId = '.$userId);
    	} else {
    		$query = $db->getQuery(true);
    		$query->insert("#__thm_reverscookings_bewertung_rezept (rezeptid, userid, bewertung) values ('.$receptId.', '.$userId.', '$rating')");
    	}
     	$db->setQuery($query);
    	$db->query();
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsrezept&id='.$receptId, false));
    
    }
    
    
}