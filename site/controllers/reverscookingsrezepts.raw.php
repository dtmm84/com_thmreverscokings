<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

jimport('joomla.application.component.controllerform');

JHTML::_('behavior.mootools');
JHTML::_('behavior.framework', true);

require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';


/**
 * Reverscookingsrezepts list controller class.
 */
class Thm_reverscookingsControllerReverscookingsrezepts extends Thm_reverscookingsController
{
	/**
	 * Proxy for getModel.
	 */
	public function &getModel($name = 'Reverscookingsrezepts', $prefix = 'Thm_reverscookingsModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		var_dump($this->items);
		return $model;
	}
	
	public function listrezepts(){
		$get = JRequest::get( 'post' );
		$inglist =$get['ingredients'];
		$inglistjson = json_decode($inglist);
		$like="";
		$inganzahl = count($inglistjson);
		for ($i=0; $i<$inganzahl; $i++){
			$like.= " i.ingname  LIKE '$inglistjson[$i]'";
			if($i<$inganzahl-1)
				$like.=" OR ";
			
		}
	
		
		$result = Thm_reverscookingsHelper::getRecipePerIng($like, $inganzahl);
		echo $result;
	}

	public function ingredientssuche(){

		$ingsearch =JRequest::getVar( 'search', '', 'get', 'cmd' ) ;
		$list = Thm_reverscookingsHelper::inglikename($ingsearch);
		$result=json_encode($list);
	   $i = count($list);
		echo $result ;
	}

}