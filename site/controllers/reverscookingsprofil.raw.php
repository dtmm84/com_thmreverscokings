<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de>
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

JHTML::_('behavior.mootools');

require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';


JHTML::_('behavior.framework', true);

/**
 * Reverscookingsprofil controller class.
 */
class Thm_reverscookingsControllerReverscookingsprofil extends Thm_reverscookingsController{
	
	function __construct() {
		$this->view_list = 'reverscookingsprofils';
		parent::__construct();
	}
	
	public function listFields() {
	
		$ingname = JRequest::getVar( 'ingname', '', 'get', 'cmd' ) ;
	
		$ingquantity = JRequest::getVar( 'ingquantity', '', 'get', 'cmd' ) ;
	
		$ingunit = JRequest::getVar( 'ingunit', '', 'get', 'cmd' ) ;
	
		$ingid = JRequest::getVar( 'ingid', '', 'get', 'cmd' ) ;
	
		$data = array($ingname,$ingquantity,$ingunit);
		$session =& JFactory::getSession();
		$rezepting = $session->get('reverscookingsprofils');
		$element= new stdClass();
		$element->id=$ingid;
		$element->value =$data;
		array_push($rezepting, $element);
		$session->set('reverscookingsprofils', $rezepting);
		$result = '<li><label>Neue Ingredients</label><table width= "50%" >';
		foreach ($rezepting as $element ){
			$index=$element->id;
			$ing=$element->value;
			if(!empty($ing[1])){
				$result.='<tr><td width= "20%">'.$ing[0].'</td>
				<td width= "20%">'.$ing[1].'</td>
				<td width= "20%">'.$ing[2].'</td>
				<td width= "10%" ><button  type=button Label = '.$ing[0].' name ='.$ing[0].' onclick= "deleteKorb('.$index.')" value = '.$ing[0].'>delete</button></td></tr>';
			}
		}
		$result .= '</table></li>';
		echo $result ;
	
	
	}
	
	public function deleteKorb (){
		$newrezepting = array();
		$ingindex = JRequest::getVar( 'korbindex', '', 'get', 'cmd' ) ;
		$session =& JFactory::getSession();
		$rezepting = $session->get('reverscookingsprofils');
		if(count($rezepting)>1){
			unset($rezepting[$ingindex]);
			$newrezepting = array_values($rezepting);
		}
	
		$session->set('reverscookingsprofils', $newrezepting);
		$result = '<li><label>Neue Ingredients</label><table width= "50%" >';
		foreach ($newrezepting as $i=>$element){
			$ing = $element->value;
			if(!is_numeric($ing[0])){
				$result.='<tr><td width= "20%">'.$ing[0].'</td>
				<td width= "20%">'.$ing[1].'</td>
				<td width= "20%">'.$ing[2].'</td>
				<td width= "10%"><button  type=button Label = '.$ing[0].' name ='.$ing[0].' onclick= "deleteKorb('.$i.')" value = '.$ing[0].'>delete</button></td></tr>';
			}
		}
		$result .= '</table></li>';
		echo $result ;
	
	}
	
	public  function deleteing(){
		$ingindex = intval(JRequest::getVar( 'ingid', '', 'get', 'cmd' )) ;
		$profilid = intval(JRequest::getVar('profilid','','get','cmd'));
		$temp = Thm_reverscookingsHelper::deletevirtualfridge($ingindex);
		$result = Thm_reverscookingsHelper::newinguserlist($profilid);
		echo $result ;
	}
	
	public function autocomplete(){
		$teilingname = JRequest::getVar( 'ingname', '', 'get', 'cmd' ) ;
		$ingredients = Thm_reverscookingsHelper::inglikename($teilingname);
		$result='<li><label> Existierende Zutaten:</label><select size="10">';
	
	foreach ($ingredients as $item){
			$value = $item->ingname.':'.$item->ingunit;
			$result.='<option  id='.$item->id.' value='.$value.'
			style="cursor:pointer" onclick="ausgewaehlt('.$item->id.')" >'.$item->ingname.':'.$item->ingunit.'</option>';
		}
		$result.='</select></li>';
		echo $result ;
	}
	
	function kochen(){
		$db = JFactory::getDBO();
		$rezept = JRequest::getVar( 'rezeptid', '', 'get', 'cmd' ) ;
		$profilid = JRequest::getVar( 'profilid', '', 'get', 'cmd' ) ;
		$portion = JRequest::getVar( 'portion', '', 'get', 'cmd' ) ;
		if(!is_numeric($rezept) || !is_numeric($profilid)){
			echo "falsche eingabe";
			return;
		}
		$rezeptquery = 'SELECT ir.* , r.portionanzahl AS portionanzahl  FROM #__thm_reverscookings_ingredients_rezept AS ir LEFT JOIN
						 #__thm_reverscookings_rezept AS r ON ir.rezeptid=r.id WHERE rezeptid='.$rezept;
		$db->setQuery($rezeptquery);
		$db->query();
		$rezepting = $db->loadObjectList();
		$updatequery = $db->setQuery(true);
		foreach ($rezepting as $element){
			$newportion =($element->quantity * $portion)/$element->portionanzahl;
			
			
 			$updatequery = 'UPDATE #__thm_reverscookings_virtual_fridge
 			SET quantity=quantity-'.$newportion.'  WHERE userid ='.$profilid.'  AND  ingid='.$element->ingid;
			$db->setQuery($updatequery);
			$db->query();
		}
		$result=Thm_reverscookingsHelper::newinguserlist($profilid);
		echo $result;
		
	
	}
	
	
}