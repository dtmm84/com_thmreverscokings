<?php
/**
 * @version     1.0.0
 * @package     com_thm_reverscookings
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License 
 *  @author Bassing <dominik.bassing@mni.thm.de>
 *  @author Seefeldt <Stefan.Seefeldt@mni.thm.de>
 *  @author Schneider <stefan.schneider@mni.thm.de>
 *  @author Omoko <guy.bertrand.omoko@hotmail.com>
 *  @author Noubeva <guylene.deutcho.noubevam@mni.thm.de>
 *  @author Timma<dieudonne.timma.meyatchie@mni.thm.de> 
 */

// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/helpers/thm_reverscookings.php';


/**
 * Reverscookingsprofil controller class.
 */
class Thm_reverscookingsControllerReverscookingsprofil extends Thm_reverscookingsController
{

	/**
	 * Method to check out an item for editing and redirect to the edit form.
	 *
	 */
	public function edit()
	{
		$app= JFactory::getApplication();

		// Get the previous edit id (if any) and the current edit id.
		$previousId = (int) $app->getUserState('com_thm_reverscookings.edit.reverscookingsprofil.id');
		$editId	= JFactory::getApplication()->input->getInt('id', null, 'array');

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.id', $editId);

		// Get the model.
		$model = $this->getModel('Reverscookingsprofil', 'Thm_reverscookingsModel');

		// Check out the item
		if ($editId) {
            $model->checkout($editId);
		}

		// Check in the previous user.
		if ($previousId) {
            $model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&layout=edit', false));
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return	void
	 */
	public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app	= JFactory::getApplication();
		$model = $this->getModel('Reverscookingsprofil', 'Thm_reverscookingsModel');

		// Get the user data.
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');
		$pid= $data['id'];
		$user_id = $data['created_by'];
		$datetime = $data['checked_out_time'];
		// Validate the posted data.
		$form = $model->getForm();
		if (!$form) {
			JError::raiseError(500, $model->getError());
			return false;
		}

		// Validate the posted data.
		$data = $model->validate($form, $data);

		// Check for errors.
		if ($data === false) {
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.data', $data);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_thm_reverscookings.edit.reverscookingsprofil.id');
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&layout=edit&id='.$id, false));
			return false;
		}

		// Attempt to save the data.
		$return	= $model->save($data);

		// Check for errors.
		if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.data', $data);

			// Redirect back to the edit screen.
			$id = (int)$app->getUserState('com_thm_reverscookings.edit.reverscookingsprofil.id');
			$this->setMessage(JText::sprintf('Save failed', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&layout=edit&id='.$id, false));
			return false;
		}

            
        // Check in the profile.
        if ($return) {
            $model->checkin($return);
        }
        if($pid ==0){
        	$maxid =Thm_reverscookingsHelper::maxid('#__thm_reverscookingsprofils');
        	$pid = $maxid[0]->id ;
        }
        Thm_reverscookingsHelper::updatetime($pid, $datetime, '#__thm_reverscookingsprofils') ;
        
        Thm_reverscookingsHelper::savebild($user_id, $pid,false);
        
        // Clear the profile id from the session.
        $app->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.id', null);

        // Redirect to the list screen.
        $this->setMessage(JText::_('Item saved successfully'));
        $this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&id='.$id, false));

		// Flush the data from the session.
		$app->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.data', null);
	}
    
    
    function cancel() {
        
		$app= JFactory::getApplication();

		//Get the edit id (if any)
		$id = (int) $app->getUserState('com_thm_reverscookings.edit.reverscookingsprofil.id');
        if ($id) {
            //Redirect back to details
            $app->setUserState('com_thm_reverscookings.edit.reverscookingsprofil.id', null);
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&id='.$id, false));
        } else {
            //Redirect back to list
			$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofils', false));
        }
        
    }
    function delete() {
    	$app= JRequest::get( 'get' );
    	$id = $app['id'];
    	$user = JFactory::getUser();
    	$result = Thm_reverscookingsHelper::hasprofil($user->id);
    	$myuserid = $result[0]->id;
    	if($myuserid == $id){
    	$db = JFactory::getDBO();
    	Thm_reverscookingsHelper::deletedata($id, "profilid", "#__thm_reverscookingsprofils_bild");
    	$query = "DELETE FROM #__thm_reverscookings_virtual_fridge WHERE userid=".$id;
    	$db->setQuery($query);
    	$db->query();
    	$query = "DELETE FROM #__thm_reverscookingsprofils WHERE id=".$id;
    	$db->setQuery($query);
    	$db->query();
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofils', false));
    	}
    
    	$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofils', false));
    }
    
    function saveing(){
    	$post = JRequest::get( 'post' );
    	if(empty($post))
    		return;
    	
    		$db = JFactory::getDBO();
    		$session =& JFactory::getSession();
    		$ingkorb = $session->get('reverscookingsprofils');
    		$rezeptingredient = $post['ingredient'];
    		$profilid = $post['profilid'];
    	
    		if(!empty($rezeptingredient)){
    			foreach ($rezeptingredient as $index => $value){
    				$quantity = intval($value);
    				$query = 'UPDATE #__thm_reverscookings_virtual_fridge  SET quantity ='.$quantity.' WHERE id ='.$index;
    				$db->setQuery($query);
    				$db->query();
    				$db->loadResult();
    			}
    		}
    		if(!empty($ingkorb)){
    			
    			Thm_reverscookingsHelper::inserVirtualfridge($profilid, $ingkorb);
    		}
    		$this->setRedirect(JRoute::_('index.php?option=com_thm_reverscookings&view=reverscookingsprofil&id='.$profilid, true));
    		
    }
    
   
    
    
}